﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccess;
namespace BusinessLogic
{
    public class HocSinh
    {
        Data da = new Data();
        public DataTable ShowHocSinh()
        {
            string sql = "SELECT MAHS as 'Mã Học Sinh',HOTEN as 'Họ và Tên', GIOITINH as 'Giới Tính',NGAYSINH as 'Ngày Sinh',DIACHI as 'Địa Chỉ',EMAIL as 'Email',LOP as 'Lớp' FROM HOCSINH";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public bool ktsiso(string MaLop)
        {
            string sql = "SELECT COUNT(*) FROM HOCSINH WHERE LOP='" + MaLop + "'";
            int siso = da.ExecuteReturnValue(sql);
            if (siso>20)
                return false;
            return true;
        }
        public DataTable LoadLop()
        {
            string sql = "SELECT TENLOP FROM LOP";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public void InsertHocSinh(string HOTEN,string GIOITINH, string NGAYSINH, string DIACHI, string EMAIL, string LOP)
        {
            string sql = "INSERT INTO HOCSINH (HOTEN,GIOITINH,NGAYSINH,DIACHI,EMAIL,LOP) VALUES (N'" + HOTEN + "','" + GIOITINH + "','" + NGAYSINH + "',N'" + DIACHI + "','" + EMAIL + "','" + LOP + "')";
            string sql1 = "INSERT INTO HOCSINH (HOTEN,GIOITINH,NGAYSINH,DIACHI,EMAIL,LOP) VALUES (N'" + HOTEN + "','" + GIOITINH + "','" + NGAYSINH + "',N'" + DIACHI + "','" + EMAIL + "'," + LOP + ")";
            if (LOP != "NULL")
                da.ExecuteNonQuery(sql);
            else da.ExecuteNonQuery(sql1);
        }
        public void UpdateHocSinh(string MaHSCu,string HOTEN,string GIOITINH, string NGAYSINH, string DIACHI, string EMAIL, string LOP)
        {
            string sql = "UPDATE HOCSINH SET HOTEN=N'" + HOTEN + "',GIOITINH='" + GIOITINH + "',NGAYSINH='" + NGAYSINH + "',DIACHI=N'" + DIACHI + "',EMAIL='" + EMAIL + "',LOP='" + LOP + "' WHERE MAHS='" + MaHSCu + "'";
            string sql1 = "UPDATE HOCSINH SET HOTEN=N'" + HOTEN + "',GIOITINH='" + GIOITINH + "',NGAYSINH='" + NGAYSINH + "',DIACHI=N'" + DIACHI + "',EMAIL='" + EMAIL + "',LOP=" + LOP + " WHERE MAHS='" + MaHSCu + "'";
            if (LOP != "DEFAULT")
                da.ExecuteNonQuery(sql);
            else da.ExecuteNonQuery(sql1);
        }
        public void DeleteHocSinh(string MaHS)
        {
            string sql = "DELETE FROM HOCSINH WHERE MAHS='"+MaHS+"'";
            da.ExecuteNonQuery(sql);
        }
        public void UpdateLopChoHS(string MaHS,string MaLop)
        {
            try
            {
                string sql = "UPDATE HOCSINH SET LOP='" + MaLop + "' WHERE MAHS='" + MaHS + "'";
                string sql1 = "UPDATE HOCSINH SET LOP=NULL WHERE MAHS='" + MaHS + "'";
                if (MaLop == "NULL")
                    da.ExecuteNonQuery(sql1);
                else da.ExecuteNonQuery(sql);
            }
            catch { }
        }
        public DataTable LoadMaHS()
        {
            string sql = "SELECT MAHS FROM HOCSINH";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable LoadTenHS()
        {
            string sql = "SELECT HOTEN FROM HOCSINH";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
    }
}
