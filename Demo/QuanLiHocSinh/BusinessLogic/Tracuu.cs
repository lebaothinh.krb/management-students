﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccess;
namespace BusinessLogic
{
    public class Tracuu
    {
        Data da = new Data();
        public DataTable TraCuuTheoMaHS(string MaHS)
        {
            string sql = "SELECT MAHS AS 'Mã Học Sinh',HOTEN as 'Họ và Tên',LOP as 'Lớp',DTB1 as 'Điểm TBHK 1',DTB2 as 'Điểm TBHK2' FROM TINHDTB('" + MaHS + "')";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable TraCuuTheoTen(string TenHS)
        {
            string sql = "SELECT MAHS AS 'Mã Học Sinh',HOTEN as 'Họ và Tên',LOP as 'Lớp',DTB1 as 'Điểm TBHK 1',DTB2 as 'Điểm TBHK2' FROM TIMTHEOTEN(N'" + TenHS + "')";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable TraCuuTheoLop(string MaLop)
        {
            string sql = "SELECT MAHS AS 'Mã Học Sinh',HOTEN as 'Họ và Tên',LOP as 'Lớp',DTB1 as 'Điểm TBHK 1',DTB2 as 'Điểm TBHK2' FROM TIMTHEOLOP ('" + MaLop + "')";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
    }
}
