﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Data;

namespace BusinessLogic
{
    public class Nhapdslop
    {
        Data da = new Data();
        public DataTable ShowSinhVienDaCoLop(string MaLop)
        {
            string sql = "SELECT * FROM HOCSINH WHERE LOP='"+MaLop+"'";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable ShowSinhVienChuaCoLop()
        {
            string sql = "SELECT * FROM HOCSINH WHERE LOP is NULL";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
    }
}
