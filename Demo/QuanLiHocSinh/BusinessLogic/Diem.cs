﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Data;

namespace BusinessLogic
{
    public class Diem
    {
        Data da = new Data();
        public DataTable ShowDiem()
        {
            string sql = "SELECT MAHS AS 'Mã Học Sinh',HOCKY as 'Học Kỳ',MON as 'Môn',DIEM15 as 'Điểm 15',DIEM1T as 'Điểm 1 Tiết',DIEMCHK as 'Điểm Cuối Kỳ' FROM BANGDIEM";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public void InsertDiem(string MaHocSinh, string HocKy, string Mon, string Diem15, string Diem1T, string DiemCHK)
        {
            string sql = "INSERT INTO BANGDIEM VALUES ('"+MaHocSinh+"','"+HocKy+"','"+Mon+"','"+Diem15+"','"+Diem1T+"','"+DiemCHK+"')";
            da.ExecuteNonQuery(sql);
        }
        public void UpdateDiem(string MaHocSinhCu, string HocKyCu, string Moncu, string MaHocSinhMoi, string HocKy, string Mon, string Diem15, string Diem1T, string DiemCHK)
        {
            string sql = "UPDATE BANGDIEM SET MAHS='"+MaHocSinhMoi+"',HOCKY='"+HocKy+"',MON='"+Mon+"',DIEM15='"+Diem15+"',DIEM1T='"+Diem1T+"',DIEMCHK='"+DiemCHK+"' WHERE MAHS='"+MaHocSinhCu+"' and HOCKY='"+HocKyCu+"' and MON='"+Moncu+"'";
            da.ExecuteNonQuery(sql);
        }
        public void DeleteDiem(string MaHocSinh, string HocKy, string Mon)
        {
            string sql = "DELETE FROM BANGDIEM WHERE MAHS='"+MaHocSinh+"' AND HOCKY='"+HocKy+"' and MON='"+Mon+"'";
            da.ExecuteNonQuery(sql);
        }
        public DataTable LoadMaHS()
        {
            string sql = "SELECT MAHS FROM HOCSINH";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable LoadMon()
        {
            string sql = "SELECT MAMH FROM MONHOC";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }

    }
}
