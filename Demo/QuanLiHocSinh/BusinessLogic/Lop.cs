﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccess;
namespace BusinessLogic
{
    public class Lop
    {
        Data da = new Data();
        public DataTable ShowLop()
        {
            string sql = "select TENLOP as 'Tên Lớp',GVCN,SISO as 'Sỉ Số',KHOILOP as 'Khối Lớp',LOAIKHOI as 'Loại Khối' from LOP";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public void InsertLop(string MaLop, string GVCN, string Siso, string KhoiLop, string LoaiKhoi)
        {
            string sql = "INSERT INTO LOP VALUES ('" + MaLop + "',N'" + GVCN + "','" + Siso + "','" + KhoiLop + "','" + LoaiKhoi + "')";
            da.ExecuteNonQuery(sql);
        }
        public void UpdateLop(string MaLopCu, string MaLop, string GVCN, string Siso, string KhoiLop, string LoaiKhoi)
        {
            string sql = "UPDATE LOP SET TENLOP='" + MaLop + "',GVCN=N'" + GVCN + "',SISO='" + Siso + "',KHOILOP='" + KhoiLop + "',LOAIKHOI='" + LoaiKhoi + "' WHERE TENLOP ='" + MaLopCu + "'";
            da.ExecuteNonQuery(sql);
        }
        public void DeleteLop(string MaLop)
        {
            string sql = "DELETE FROM LOP WHERE TENLOP='"+MaLop+"'";
            da.ExecuteNonQuery(sql);
        }
        public DataTable DeleteHocSinhXoa(string MaLop)
        {
            DataTable dt = new DataTable();
            string sql = "SELECT MAHS from HOCSINH WHERE LOP='" + MaLop + "'";
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable LoadMaLop()
        {
            string sql = "SELECT TENLOP FROM LOP";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public DataTable LoadSiSo(string MaLop)
        {
            string sql = "SELECT SISO FROM LOP WHERE TENLOP='" + MaLop + "'";
            return da.GetTable(sql);
        }
    }
}
