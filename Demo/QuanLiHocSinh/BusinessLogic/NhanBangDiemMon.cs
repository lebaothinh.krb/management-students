﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccess;

namespace BusinessLogic
{
    public class NhanBangDiemMon
    {
        Data da = new Data();
        public DataTable LayBangDiem(string MaLop, string HocKy, string Mon)
        {
            string sql = "SELECT HOCSINH.MAHS AS 'Mã Học Sinh',HOCSINH.HOTEN AS 'Họ và Tên',BANGDIEM.DIEM15 AS 'Điểm 15 phút',BANGDIEM.DIEM1T AS 'Điểm 1 Tiết',BANGDIEM.DIEMCHK AS 'Điểm Cuối Kì' from BANGDIEM,HOCSINH,LOP WHERE BANGDIEM.MAHS = HOCSINH.MAHS AND HOCSINH.LOP = LOP.TENLOP AND LOP='"+MaLop+"' and HOCKY='"+HocKy+"' and MON='"+Mon+"'";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
    }
}
