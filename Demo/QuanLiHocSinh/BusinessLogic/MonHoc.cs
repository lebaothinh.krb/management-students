﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccess;

namespace BusinessLogic
{
    public class MonHoc
    {
        Data da = new Data();
        public DataTable ShowMonHoc()
        {
            string sql = "SELECT MAMH as 'Mã Môn Học',TENMH as 'Tên Môn Học' FROM MONHOC";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
        public void InsertMonHoc(string MaMonHoc, string TenMonHoc)
        {
            string sql = "INSERT INTO MONHOC VALUES ('"+MaMonHoc+"',N'"+TenMonHoc+"')";
            da.ExecuteNonQuery(sql);
        }
        public void UpdateMonHoc(string MaMonHocCu, string MaMonHocMoi, string TenMonHoc)
        {
            string sql = "UPDATE MONHOC SET MAMH='"+MaMonHocMoi+"',TENMH=N'"+TenMonHoc+"' WHERE MAMH='"+MaMonHocCu+"'";
            da.ExecuteNonQuery(sql);
        }
        public void DeleteMonHoc(string MaMonHoc)
        {
            string sql = "DELETE from MONHOC WHERE MAMH='"+MaMonHoc+"'";
            da.ExecuteNonQuery(sql);
        }
        public DataTable LoadMaMon()
        {
            string sql = "SELECT MAMH FROM MONHOC";
            DataTable dt = new DataTable();
            dt = da.GetTable(sql);
            return dt;
        }
    }
}
