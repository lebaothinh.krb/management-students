﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

// Nhiệm vụ của lớp này là kết nối cơ sở dữ liệu và thực thi lệnh sql:
// Lệnh SQL có 2 dạng:
//- Không trả về bảng
//- Trả về bảng
namespace DataAccess
{
    public class Data
    {
        public SqlConnection getConnect()
        {
            return new SqlConnection(@"Data Source=MYCAY-PC\SQLEXPRESS;Initial Catalog=QL_HocSinh;Integrated Security=True");
        }
        //lệnh SQL trả về một bảng
        public DataTable GetTable(string sql)
        {
            SqlConnection con = getConnect();
            SqlDataAdapter ad = new SqlDataAdapter(sql, con);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            return dt;
        }
        // lệnh SQL không trả về
        public void ExecuteNonQuery(string sql)
        {
            SqlConnection con = getConnect();
            con.Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public int ExecuteReturnValue(string sql)
        {
            int value = 0;
            SqlConnection con = getConnect();
            con.Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            value = (int)cmd.ExecuteScalar();
            con.Close();
            return value;
        }
    }
}
