﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BusinessLogic;

namespace QuanLiHocSinh
{
    public partial class frmQuanLiHoSoHS : Form
    {
        public frmQuanLiHoSoHS()
        {
            InitializeComponent();
        }
        HocSinh hs = new HocSinh();
        bool Them = true, CapNhat = true;

        public void MoKhoa()
        {
            txtHoten.ReadOnly = false;
            txtNgaySinh.ReadOnly = false;
            txtEmail.ReadOnly = false;
            cbGioiTinh.Enabled = true;
            txtDiaChi.ReadOnly = false;
            txtEmail.ReadOnly = false;
            cbLop.Enabled = true;
        }

        public void Khoa()
        {
            txtHoten.ReadOnly = true;
            txtNgaySinh.ReadOnly = true;
            txtEmail.ReadOnly = true;
            cbGioiTinh.Enabled = false;
            txtDiaChi.ReadOnly = true;
            txtEmail.ReadOnly = true;
            cbLop.Enabled = false;
        }
        string[] ns = new string[3];
        public bool rangbuoc(ref bool CS)
        {
            if (txtHoten.Text == "")
            {
                MessageBox.Show("Bạn Chưa Nhập Tên!");
                CS = false;
                return false;
            }
            if (txtNgaySinh.Text == "")
            {
                MessageBox.Show("Bạn Chưa Nhập Ngày Sinh!");
                CS = false;
                return false;
            }
            int lop = -1, tuoi = 0;
            try
            {
                ns = txtNgaySinh.Text.Split('/');
                tuoi = DateTime.Now.Year - Convert.ToInt32(ns[2].Substring(0, 4));
            }
            catch
            {
                MessageBox.Show("Vui lòng kiểm tra lại trường Ngày Sinh (theo định dạng ngày/tháng/năm)");
                CS = false;
                return false;
            }
            if (cbLop.Text != "" && cbLop.Text != "DEFAULT")
            {
                if (hs.ktsiso(cbLop.Text) == false)
                {
                    MessageBox.Show("Sỉ Số Lớp " + cbLop.Text + " Không Được Quá 20!");
                    CS = false;
                    return false;
                }
                try
                {
                    lop = Convert.ToInt32(cbLop.Text.Substring(0, 2));
                }
                catch
                {
                    MessageBox.Show("Vui lòng kiểm tra lại trường lớp, có thể sai!");
                    CS = false;
                    return false;
                }
            }
            else
            {
                cbLop.Text = "NULL";
            }
            if (tuoi <= 20 && tuoi >= 15)
            {
                if (cbLop.Text != "")
                {
                    if (tuoi < 18 && lop == 12)
                    {
                        MessageBox.Show("Lớp 12 phải từ 18 tuổi trở lên!");
                        txtNgaySinh.Focus();
                        CS = false;
                        return false;
                    }
                    if (tuoi > 17 && lop == 10)
                    {
                        MessageBox.Show("Lớp 10 phải dưới 18 tuổi");
                        txtNgaySinh.Focus();
                        CS = false;
                        return false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Tuổi Của Học Sinh Phải Từ 15 đến 20");
                CS = false;
                return false;
            }
            if (txtEmail.Text == "")
            {
                MessageBox.Show("Bạn Chưa Nhập Email!");
                CS = false;
                return false;
            }
            if (cbGioiTinh.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn Chưa Chọn Giới Tính Đúng!");
                CS = false;
                return false;
            }
            if (txtHoten.Text == "")
            {
                MessageBox.Show("Bạn Chưa Nhập Tên!");
                CS = false;
                return false;
            }
            if (txtDiaChi.Text == "")
            {
                MessageBox.Show("Bạn Chưa Nhập Địa Chỉ!");
                CS = false;
                return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            Them = !Them;
            if (Them == false)
            {
                MoKhoa();
                btnSua.Enabled = false;
                btnXoa.Enabled = false;
                dataGridViewHocSinh.Enabled = false;
                btnHuyThem.Visible = true;
                btnThem.BackColor = Color.LightGray;
                btnXoaTrang_Click(null, null);
            }
            else
            {
                if (rangbuoc(ref Them) == false) return;
                try
                {
                    //MessageBox.Show(this.txtHoten.Text + " - " + (this.cbGioiTinh.Text == "Nam" ? "1" : "0")+ "  -  " + this.txtNgaySinh.Text + " - " + this.txtDiaChi.Text + " - " + this.txtEmail.Text + " - " + this.cbLop.Text);
                    hs.InsertHocSinh(this.txtHoten.Text, (this.cbGioiTinh.Text == "Nam" ? "1" : "0").ToString(), (ns[1] + "/" + ns[0] + "/" + ns[2].Substring(0, 4)), this.txtDiaChi.Text, this.txtEmail.Text, this.cbLop.Text);
                    MessageBox.Show("Thêm Thành Công!");
                    Khoa();
                    btnThem.BackColor = Color.Transparent;
                    btnSua.Enabled = true;
                    btnXoa.Enabled = true;
                    btnHuyThem.Visible = false;
                    dataGridViewHocSinh.Enabled = true;
                    frmQuanLiHoSoSV_Load(null, null);
                }
                catch
                {
                    MessageBox.Show("Vui lòng kiểm tra lại trường Ngày Sinh (theo định dạng ngày tháng năm)\nTrường Giới Tính (Chỉ có hai giới tính nam nữ)\nTrường Lớp có thể không có lớp "+cbLop.Text+"!");
                    Them = false;
                }
            }
        }

        private void frmQuanLiHoSoSV_Load(object sender, EventArgs e)
        {
            Khoa();
            dataGridViewHocSinh.DataSource = hs.ShowHocSinh();
            dataGridViewHocSinh.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            cbLop.DataSource = hs.LoadLop();
            cbLop.DisplayMember = "TENLOP";
            cbLop.ValueMember = "TENLOP";
        }

        private void dataGridViewHocSinh_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int dong = e.RowIndex;
                txtMaHS.Text = dataGridViewHocSinh.Rows[dong].Cells[0].Value.ToString();
                txtHoten.Text = dataGridViewHocSinh.Rows[dong].Cells[1].Value.ToString();
                if (dataGridViewHocSinh.Rows[dong].Cells[2].Value.ToString() == "True")
                    cbGioiTinh.Text = "Nam";
                else
                    cbGioiTinh.Text = "Nữ";
                txtNgaySinh.Text = dataGridViewHocSinh.Rows[dong].Cells[3].Value.ToString();
                txtDiaChi.Text = dataGridViewHocSinh.Rows[dong].Cells[4].Value.ToString();
                txtEmail.Text = dataGridViewHocSinh.Rows[dong].Cells[5].Value.ToString();
                cbLop.Text = dataGridViewHocSinh.Rows[dong].Cells[6].Value.ToString();
            }
            catch
            { }
        }

        private void btnXoaTrang_Click(object sender, EventArgs e)
        {
            MaHocSinhXoa = txtMaHS.Text;
            txtMaHS.Text = "";
            txtHoten.Text = "";
            txtNgaySinh.Text = "";
            txtEmail.Text = "";
            cbGioiTinh.Text = "";
            txtDiaChi.Text = "";
            txtEmail.Text = "";
            cbLop.Text = "";
        }
        string MaHocSinhXoa = "";
        private void btnSua_Click(object sender, EventArgs e)
        {
            CapNhat = !CapNhat;
            string MaHocSinhCu = MaHocSinhXoa;
            if (txtMaHS.Text != "")
                MaHocSinhCu = txtMaHS.Text;
            else txtMaHS.Text = MaHocSinhCu;
            if (CapNhat == false)
            {
                dataGridViewHocSinh.Enabled = false;
                btnThem.Enabled = false;
                btnXoa.Enabled = false;
                btnHuySua.Visible = true;
                btnSua.BackColor = Color.LightGray;
                MoKhoa();
            }
            else
            {
                if (rangbuoc(ref CapNhat) == false) return;
                try
                {
                    if (cbLop.Text == "") cbLop.Text = "DEFAULT";
                    //MessageBox.Show(MaHocSinhCu+" - "+ this.txtHoten.Text+" - "+ (this.cbGioiTinh.Text == "Nam" ? "1" : "0").ToString()+" - "+ this.txtNgaySinh.Text+" -  "+ this.txtDiaChi.Text+" - "+ this.txtEmail.Text+" - "+ this.cbLop.Text);
                    hs.UpdateHocSinh(MaHocSinhCu, this.txtHoten.Text, (this.cbGioiTinh.Text == "Nam" ? "1" : "0").ToString(),(ns[1]+"/"+ns[0]+"/"+ns[2].Substring(0,4)), this.txtDiaChi.Text, this.txtEmail.Text, this.cbLop.Text);
                    MessageBox.Show("Cập Nhật Thành Công!");
                    Khoa();
                    dataGridViewHocSinh.Enabled = true;
                    btnSua.BackColor = Color.Transparent;
                    btnThem.Enabled = true;
                    btnXoa.Enabled = true;
                    btnHuySua.Visible = false;
                    frmQuanLiHoSoSV_Load(null, null);
                }
                catch
                {
                    MessageBox.Show("Vui lòng kiểm tra lại trường Ngày Sinh (theo định dạng ngày tháng năm)\nTrường Giới Tính (Chỉ có hai giới tính nam nữ)\nTrường Lớp có thể không có lớp " + cbLop.Text + "!");
                    CapNhat = false;
                }
            } 
        }

        private void btnHuyThem_Click(object sender, EventArgs e)
        {
            Them = true;
            btnHuyThem.Visible = false;
            btnSua.Enabled = true;
            btnXoa.Enabled = true;
            btnThem.BackColor = Color.Transparent;
            frmQuanLiHoSoSV_Load(null, e);
            dataGridViewHocSinh.Enabled = true;
        }

        private void btnHuySua_Click(object sender, EventArgs e)
        {
            CapNhat = true;
            btnHuySua.Visible = false;
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
            btnSua.BackColor = Color.Transparent;
            frmQuanLiHoSoSV_Load(null, e);
            dataGridViewHocSinh.Enabled = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaHS.Text != "")
                MaHocSinhXoa = txtMaHS.Text;
            DialogResult KQ = MessageBox.Show("Bạn Có Chắc Chắn Muốn Xóa Học Sinh " + MaHocSinhXoa + "?", "Quyết Định!", MessageBoxButtons.YesNo);
            if (KQ == DialogResult.Yes)
            {
                hs.DeleteHocSinh(MaHocSinhXoa);
                MessageBox.Show("Xóa Thành Công!");
                frmQuanLiHoSoSV_Load(null, e);
            }
        }

        private void dataGridViewHocSinh_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
