﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BusinessLogic;

namespace QuanLiHocSinh
{
    public partial class frmTraCuuSinhVien : Form
    {
        public frmTraCuuSinhVien()
        {
            InitializeComponent();
        }
        Lop loph = new Lop();
        HocSinh hs = new HocSinh();
        Tracuu tracuu = new Tracuu();
        private void frmTraCuuSinhVien_Load(object sender, EventArgs e)
        {
            lblMaHocSinh.Visible = false ;
            cbMaHS.Visible = false;
            lblTenHocSinh.Visible = false;
            cbTenHocSinh.Visible = false;
            lblLop.Visible = false;
            cbLop.Visible = false;
            cbLop.DataSource = loph.LoadMaLop();
            cbLop.DisplayMember = "TENLOP";
            cbLop.ValueMember = "TENLOP";
            cbMaHS.DataSource = hs.LoadMaHS();
            cbMaHS.DisplayMember = "MAHS";
            cbMaHS.ValueMember = "MAHS";
            cbTenHocSinh.DataSource = hs.LoadTenHS();
            cbTenHocSinh.DisplayMember = "HOTEN";
            cbTenHocSinh.ValueMember = "HOTEN";
        }

        private void cbTracuu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTracuu.SelectedIndex == 0)
            {
                lblMaHocSinh.Visible = true;
                cbMaHS.Visible = true;
                lblTenHocSinh.Visible = false;
                cbTenHocSinh.Visible = false;
                lblLop.Visible = false;
                cbLop.Visible = false;
            }
            if (cbTracuu.SelectedIndex == 1)
            {
                lblMaHocSinh.Visible = false;
                cbMaHS.Visible = false;
                lblTenHocSinh.Visible = true;
                cbTenHocSinh.Visible = true;
                lblLop.Visible = false;
                cbLop.Visible = false;
            }
            if (cbTracuu.SelectedIndex == 2)
            {
                lblMaHocSinh.Visible = false;
                cbMaHS.Visible = false;
                lblTenHocSinh.Visible = false;
                cbTenHocSinh.Visible = false;
                lblLop.Visible = true;
                cbLop.Visible = true;
            }
        }

        private void btnTraCuu_Click(object sender, EventArgs e)
        {
            if (cbTracuu.SelectedIndex == 0)
            {
                    try
                    {
                        dataGridViewTracuu.DataSource = tracuu.TraCuuTheoMaHS(cbMaHS.SelectedValue.ToString());
                        dataGridViewTracuu.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    }
                    catch
                    {
                        MessageBox.Show("Không Có Học Sinh Này");
                    }
            }
            if (cbTracuu.SelectedIndex == 1)
            {
                try
                {
                    dataGridViewTracuu.DataSource = tracuu.TraCuuTheoTen(cbTenHocSinh.Text);
                    dataGridViewTracuu.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
                catch
                {
                    MessageBox.Show("Không Có Học Sinh Này!");
                }
            }
            if (cbTracuu.SelectedIndex == 2)
            {
                    try
                    {
                        dataGridViewTracuu.DataSource = tracuu.TraCuuTheoLop(cbLop.SelectedValue.ToString());
                        dataGridViewTracuu.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    }
                    catch
                    {
                        MessageBox.Show("Không Có Lớp Này");
                    }
            }
        }
    }
}
