﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogic;
using System.Data.SqlClient;

namespace QuanLiHocSinh
{
    public partial class frmNhanBangDiemMon : Form
    {
        public frmNhanBangDiemMon()
        {
            InitializeComponent();
        }
        Lop loph = new Lop();
        MonHoc mon = new MonHoc();
        NhanBangDiemMon nhanbd = new NhanBangDiemMon();
        private void frmNhanBangDiemMon_Load(object sender, EventArgs e)
        {
            cbLop.DataSource = loph.LoadMaLop();
            cbLop.DisplayMember = "TENLOP";
            cbLop.ValueMember = "TENLOP";
            cbMon.DataSource = mon.LoadMaMon();
            cbMon.DisplayMember = "MAMH";
            cbMon.ValueMember = "MAMH";
            cbHocKy.SelectedIndex = 0;
        }

        private void btnTraCuu_Click(object sender, EventArgs e)
        {
            if (cbLop.SelectedIndex == -1)
            {
                MessageBox.Show("Mã Lớp Không Tồn Tại!");
                return;
            }
            if (cbMon.SelectedIndex == -1)
            {
                MessageBox.Show("Mã Môn Không Tồn Tại!");
                return;
            }
            if (cbHocKy.SelectedIndex == -1)
            {
                MessageBox.Show("Học Kỳ Không Tồn Tại!");
                return;
            }
            try
            {
                dataGridViewBangDiem.DataSource = nhanbd.LayBangDiem(this.cbLop.Text, this.cbHocKy.Text, this.cbMon.Text);
                dataGridViewBangDiem.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch
            {
                MessageBox.Show("Chọn sai!");
            }
        }
    }
}
