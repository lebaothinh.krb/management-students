﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLiHocSinh
{
    public partial class frmChinh : Form
    {
        public frmChinh()
        {
            InitializeComponent();
        }

        private void frmChinh_Load(object sender, EventArgs e)
        {

        }

        private void btnQuanLySinhVien_Click(object sender, EventArgs e)
        {
            frmQuanLiHoSoHS QuanLyHoSoHocSinh = new frmQuanLiHoSoHS();
            QuanLyHoSoHocSinh.Show();
        }

        private void btnQuanLyMonHoc_Click(object sender, EventArgs e)
        {
            frmQuanLyMonHoc QuanLyMonHoc = new frmQuanLyMonHoc();
            QuanLyMonHoc.Show();
        }

        private void btnQuanLyDiem_Click(object sender, EventArgs e)
        {
            frmQuanLyDiem QuanLyDiem = new frmQuanLyDiem();
            QuanLyDiem.Show();
        }

        private void QuanLyLop_Click(object sender, EventArgs e)
        {
            frmQuanLyLop QuanLyLop = new frmQuanLyLop();
            QuanLyLop.Show();
        }

        private void btnTraCuu_Click(object sender, EventArgs e)
        {
            frmTraCuuSinhVien TraCuu = new frmTraCuuSinhVien();
            TraCuu.Show();
        }

        private void btnNhapBangDiem_Click(object sender, EventArgs e)
        {
            frmNhanBangDiemMon NhanBangDiem = new frmNhanBangDiemMon();
            NhanBangDiem.Show();
        }

        private void btnNhapDanhSachLop_Click(object sender, EventArgs e)
        {
            frmNhapDanhSachLop NhapDSLop = new frmNhapDanhSachLop();
            NhapDSLop.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
