﻿namespace QuanLiHocSinh
{
    partial class frmQuanLiHoSoHS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelbuttionsHocSinh = new System.Windows.Forms.Panel();
            this.btnHuySua = new System.Windows.Forms.Button();
            this.btnHuyThem = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnXoaTrang = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.dataGridViewHocSinh = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbGioiTinh = new System.Windows.Forms.ComboBox();
            this.cbLop = new System.Windows.Forms.ComboBox();
            this.lblLop = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblNgaySinh = new System.Windows.Forms.Label();
            this.lblDiaChi = new System.Windows.Forms.Label();
            this.lblGioiTinh = new System.Windows.Forms.Label();
            this.lblHoten = new System.Windows.Forms.Label();
            this.lblMaHS = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtNgaySinh = new System.Windows.Forms.TextBox();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtHoten = new System.Windows.Forms.TextBox();
            this.txtMaHS = new System.Windows.Forms.TextBox();
            this.panelbuttionsHocSinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHocSinh)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelbuttionsHocSinh
            // 
            this.panelbuttionsHocSinh.Controls.Add(this.btnHuySua);
            this.panelbuttionsHocSinh.Controls.Add(this.btnHuyThem);
            this.panelbuttionsHocSinh.Controls.Add(this.btnThem);
            this.panelbuttionsHocSinh.Controls.Add(this.btnXoa);
            this.panelbuttionsHocSinh.Controls.Add(this.btnXoaTrang);
            this.panelbuttionsHocSinh.Controls.Add(this.btnSua);
            this.panelbuttionsHocSinh.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelbuttionsHocSinh.Location = new System.Drawing.Point(407, 0);
            this.panelbuttionsHocSinh.Name = "panelbuttionsHocSinh";
            this.panelbuttionsHocSinh.Size = new System.Drawing.Size(277, 148);
            this.panelbuttionsHocSinh.TabIndex = 6;
            // 
            // btnHuySua
            // 
            this.btnHuySua.Location = new System.Drawing.Point(145, 19);
            this.btnHuySua.Name = "btnHuySua";
            this.btnHuySua.Size = new System.Drawing.Size(97, 41);
            this.btnHuySua.TabIndex = 4;
            this.btnHuySua.Text = "Hủy Cập Nhật";
            this.btnHuySua.UseVisualStyleBackColor = true;
            this.btnHuySua.Visible = false;
            this.btnHuySua.Click += new System.EventHandler(this.btnHuySua_Click);
            // 
            // btnHuyThem
            // 
            this.btnHuyThem.Location = new System.Drawing.Point(145, 19);
            this.btnHuyThem.Name = "btnHuyThem";
            this.btnHuyThem.Size = new System.Drawing.Size(97, 41);
            this.btnHuyThem.TabIndex = 3;
            this.btnHuyThem.Text = "Hủy Thêm ";
            this.btnHuyThem.UseVisualStyleBackColor = true;
            this.btnHuyThem.Visible = false;
            this.btnHuyThem.Click += new System.EventHandler(this.btnHuyThem_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(28, 19);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(97, 41);
            this.btnThem.TabIndex = 6;
            this.btnThem.Text = "Thêm Học Sinh";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(145, 19);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(97, 41);
            this.btnXoa.TabIndex = 0;
            this.btnXoa.Text = "Xóa Học Sinh";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnXoaTrang
            // 
            this.btnXoaTrang.Location = new System.Drawing.Point(145, 77);
            this.btnXoaTrang.Name = "btnXoaTrang";
            this.btnXoaTrang.Size = new System.Drawing.Size(97, 41);
            this.btnXoaTrang.TabIndex = 8;
            this.btnXoaTrang.Text = "Xóa Trắng";
            this.btnXoaTrang.UseVisualStyleBackColor = true;
            this.btnXoaTrang.Click += new System.EventHandler(this.btnXoaTrang_Click);
            // 
            // btnSua
            // 
            this.btnSua.Location = new System.Drawing.Point(28, 77);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(97, 41);
            this.btnSua.TabIndex = 7;
            this.btnSua.Text = "Cập Nhật";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // dataGridViewHocSinh
            // 
            this.dataGridViewHocSinh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHocSinh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewHocSinh.Location = new System.Drawing.Point(0, 148);
            this.dataGridViewHocSinh.Name = "dataGridViewHocSinh";
            this.dataGridViewHocSinh.ReadOnly = true;
            this.dataGridViewHocSinh.Size = new System.Drawing.Size(684, 214);
            this.dataGridViewHocSinh.TabIndex = 5;
            this.dataGridViewHocSinh.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHocSinh_CellContentClick);
            this.dataGridViewHocSinh.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHocSinh_RowEnter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbGioiTinh);
            this.panel1.Controls.Add(this.cbLop);
            this.panel1.Controls.Add(this.lblLop);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.lblNgaySinh);
            this.panel1.Controls.Add(this.lblDiaChi);
            this.panel1.Controls.Add(this.lblGioiTinh);
            this.panel1.Controls.Add(this.lblHoten);
            this.panel1.Controls.Add(this.lblMaHS);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.txtNgaySinh);
            this.panel1.Controls.Add(this.txtDiaChi);
            this.panel1.Controls.Add(this.txtHoten);
            this.panel1.Controls.Add(this.txtMaHS);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 148);
            this.panel1.TabIndex = 7;
            // 
            // cbGioiTinh
            // 
            this.cbGioiTinh.FormattingEnabled = true;
            this.cbGioiTinh.Items.AddRange(new object[] {
            "Nam",
            "Nữ"});
            this.cbGioiTinh.Location = new System.Drawing.Point(278, 15);
            this.cbGioiTinh.Name = "cbGioiTinh";
            this.cbGioiTinh.Size = new System.Drawing.Size(100, 21);
            this.cbGioiTinh.TabIndex = 3;
            // 
            // cbLop
            // 
            this.cbLop.FormattingEnabled = true;
            this.cbLop.Location = new System.Drawing.Point(278, 108);
            this.cbLop.Name = "cbLop";
            this.cbLop.Size = new System.Drawing.Size(100, 21);
            this.cbLop.TabIndex = 5;
            // 
            // lblLop
            // 
            this.lblLop.AutoSize = true;
            this.lblLop.Location = new System.Drawing.Point(211, 112);
            this.lblLop.Name = "lblLop";
            this.lblLop.Size = new System.Drawing.Size(25, 13);
            this.lblLop.TabIndex = 1;
            this.lblLop.Text = "Lớp";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(13, 112);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 1;
            this.lblEmail.Text = "Email";
            // 
            // lblNgaySinh
            // 
            this.lblNgaySinh.AutoSize = true;
            this.lblNgaySinh.Location = new System.Drawing.Point(13, 81);
            this.lblNgaySinh.Name = "lblNgaySinh";
            this.lblNgaySinh.Size = new System.Drawing.Size(56, 13);
            this.lblNgaySinh.TabIndex = 1;
            this.lblNgaySinh.Text = "Ngày Sinh";
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.AutoSize = true;
            this.lblDiaChi.Location = new System.Drawing.Point(211, 51);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(41, 13);
            this.lblDiaChi.TabIndex = 1;
            this.lblDiaChi.Text = "Địa Chỉ";
            // 
            // lblGioiTinh
            // 
            this.lblGioiTinh.AutoSize = true;
            this.lblGioiTinh.Location = new System.Drawing.Point(211, 19);
            this.lblGioiTinh.Name = "lblGioiTinh";
            this.lblGioiTinh.Size = new System.Drawing.Size(51, 13);
            this.lblGioiTinh.TabIndex = 1;
            this.lblGioiTinh.Text = "Giới Tính";
            // 
            // lblHoten
            // 
            this.lblHoten.AutoSize = true;
            this.lblHoten.Location = new System.Drawing.Point(13, 51);
            this.lblHoten.Name = "lblHoten";
            this.lblHoten.Size = new System.Drawing.Size(59, 13);
            this.lblHoten.TabIndex = 1;
            this.lblHoten.Text = "Họ Và Tên";
            // 
            // lblMaHS
            // 
            this.lblMaHS.AutoSize = true;
            this.lblMaHS.Location = new System.Drawing.Point(13, 19);
            this.lblMaHS.Name = "lblMaHS";
            this.lblMaHS.Size = new System.Drawing.Size(69, 13);
            this.lblMaHS.TabIndex = 1;
            this.lblMaHS.Text = "Mã Học Sinh";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(88, 108);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(100, 20);
            this.txtEmail.TabIndex = 2;
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.Location = new System.Drawing.Point(88, 77);
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Size = new System.Drawing.Size(100, 20);
            this.txtNgaySinh.TabIndex = 1;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(278, 47);
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(100, 47);
            this.txtDiaChi.TabIndex = 4;
            // 
            // txtHoten
            // 
            this.txtHoten.Location = new System.Drawing.Point(88, 47);
            this.txtHoten.Name = "txtHoten";
            this.txtHoten.Size = new System.Drawing.Size(100, 20);
            this.txtHoten.TabIndex = 0;
            // 
            // txtMaHS
            // 
            this.txtMaHS.Location = new System.Drawing.Point(88, 15);
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.ReadOnly = true;
            this.txtMaHS.Size = new System.Drawing.Size(100, 20);
            this.txtMaHS.TabIndex = 0;
            // 
            // frmQuanLiHoSoHS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelbuttionsHocSinh);
            this.Controls.Add(this.dataGridViewHocSinh);
            this.MaximumSize = new System.Drawing.Size(700, 400);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "frmQuanLiHoSoHS";
            this.Text = "Quản Lý Hồ Sơ Học Sinh";
            this.Load += new System.EventHandler(this.frmQuanLiHoSoSV_Load);
            this.panelbuttionsHocSinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHocSinh)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelbuttionsHocSinh;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.DataGridView dataGridViewHocSinh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLop;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblNgaySinh;
        private System.Windows.Forms.Label lblDiaChi;
        private System.Windows.Forms.Label lblGioiTinh;
        private System.Windows.Forms.Label lblHoten;
        private System.Windows.Forms.Label lblMaHS;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtNgaySinh;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtHoten;
        private System.Windows.Forms.TextBox txtMaHS;
        private System.Windows.Forms.Button btnXoaTrang;
        private System.Windows.Forms.ComboBox cbLop;
        private System.Windows.Forms.ComboBox cbGioiTinh;
        private System.Windows.Forms.Button btnHuyThem;
        private System.Windows.Forms.Button btnHuySua;

    }
}