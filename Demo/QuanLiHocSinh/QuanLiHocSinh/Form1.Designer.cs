﻿namespace QuanLiHocSinh
{
    partial class frmChinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuanLyHocSinh = new System.Windows.Forms.Button();
            this.btnTraCuu = new System.Windows.Forms.Button();
            this.btnNhapBangDiem = new System.Windows.Forms.Button();
            this.btnNhapDanhSachLop = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnQuanLyMonHoc = new System.Windows.Forms.Button();
            this.btnQuanLyDiem = new System.Windows.Forms.Button();
            this.QuanLyLop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnQuanLyHocSinh
            // 
            this.btnQuanLyHocSinh.Location = new System.Drawing.Point(17, 22);
            this.btnQuanLyHocSinh.Name = "btnQuanLyHocSinh";
            this.btnQuanLyHocSinh.Size = new System.Drawing.Size(212, 44);
            this.btnQuanLyHocSinh.TabIndex = 0;
            this.btnQuanLyHocSinh.Text = "Quản Lý Học Sinh";
            this.btnQuanLyHocSinh.UseVisualStyleBackColor = true;
            this.btnQuanLyHocSinh.Click += new System.EventHandler(this.btnQuanLySinhVien_Click);
            // 
            // btnTraCuu
            // 
            this.btnTraCuu.Location = new System.Drawing.Point(301, 22);
            this.btnTraCuu.Name = "btnTraCuu";
            this.btnTraCuu.Size = new System.Drawing.Size(212, 44);
            this.btnTraCuu.TabIndex = 4;
            this.btnTraCuu.Text = "Tra Cứu Học Sinh";
            this.btnTraCuu.UseVisualStyleBackColor = true;
            this.btnTraCuu.Click += new System.EventHandler(this.btnTraCuu_Click);
            // 
            // btnNhapBangDiem
            // 
            this.btnNhapBangDiem.Location = new System.Drawing.Point(301, 93);
            this.btnNhapBangDiem.Name = "btnNhapBangDiem";
            this.btnNhapBangDiem.Size = new System.Drawing.Size(212, 44);
            this.btnNhapBangDiem.TabIndex = 5;
            this.btnNhapBangDiem.Text = "Nhận Bảng Điểm Môn";
            this.btnNhapBangDiem.UseVisualStyleBackColor = true;
            this.btnNhapBangDiem.Click += new System.EventHandler(this.btnNhapBangDiem_Click);
            // 
            // btnNhapDanhSachLop
            // 
            this.btnNhapDanhSachLop.Location = new System.Drawing.Point(301, 164);
            this.btnNhapDanhSachLop.Name = "btnNhapDanhSachLop";
            this.btnNhapDanhSachLop.Size = new System.Drawing.Size(212, 44);
            this.btnNhapDanhSachLop.TabIndex = 6;
            this.btnNhapDanhSachLop.Text = "Nhập Danh Sách Lớp";
            this.btnNhapDanhSachLop.UseVisualStyleBackColor = true;
            this.btnNhapDanhSachLop.Click += new System.EventHandler(this.btnNhapDanhSachLop_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(301, 239);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(212, 44);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnQuanLyMonHoc
            // 
            this.btnQuanLyMonHoc.Location = new System.Drawing.Point(17, 93);
            this.btnQuanLyMonHoc.Name = "btnQuanLyMonHoc";
            this.btnQuanLyMonHoc.Size = new System.Drawing.Size(212, 44);
            this.btnQuanLyMonHoc.TabIndex = 1;
            this.btnQuanLyMonHoc.Text = "Quản Lý Môn Học";
            this.btnQuanLyMonHoc.UseVisualStyleBackColor = true;
            this.btnQuanLyMonHoc.Click += new System.EventHandler(this.btnQuanLyMonHoc_Click);
            // 
            // btnQuanLyDiem
            // 
            this.btnQuanLyDiem.Location = new System.Drawing.Point(17, 164);
            this.btnQuanLyDiem.Name = "btnQuanLyDiem";
            this.btnQuanLyDiem.Size = new System.Drawing.Size(212, 44);
            this.btnQuanLyDiem.TabIndex = 2;
            this.btnQuanLyDiem.Text = "Quản Lý Điểm";
            this.btnQuanLyDiem.UseVisualStyleBackColor = true;
            this.btnQuanLyDiem.Click += new System.EventHandler(this.btnQuanLyDiem_Click);
            // 
            // QuanLyLop
            // 
            this.QuanLyLop.Location = new System.Drawing.Point(17, 239);
            this.QuanLyLop.Name = "QuanLyLop";
            this.QuanLyLop.Size = new System.Drawing.Size(212, 44);
            this.QuanLyLop.TabIndex = 3;
            this.QuanLyLop.Text = "Quản Lý Lớp";
            this.QuanLyLop.UseVisualStyleBackColor = true;
            this.QuanLyLop.Click += new System.EventHandler(this.QuanLyLop_Click);
            // 
            // frmChinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::QuanLiHocSinh.Properties.Resources.pexels_photo_14676;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(533, 307);
            this.Controls.Add(this.QuanLyLop);
            this.Controls.Add(this.btnQuanLyDiem);
            this.Controls.Add(this.btnQuanLyMonHoc);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnNhapDanhSachLop);
            this.Controls.Add(this.btnNhapBangDiem);
            this.Controls.Add(this.btnTraCuu);
            this.Controls.Add(this.btnQuanLyHocSinh);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(549, 345);
            this.MinimumSize = new System.Drawing.Size(549, 345);
            this.Name = "frmChinh";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chương Trình Quản Lý Học Sinh";
            this.Load += new System.EventHandler(this.frmChinh_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnQuanLyHocSinh;
        private System.Windows.Forms.Button btnTraCuu;
        private System.Windows.Forms.Button btnNhapBangDiem;
        private System.Windows.Forms.Button btnNhapDanhSachLop;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnQuanLyMonHoc;
        private System.Windows.Forms.Button btnQuanLyDiem;
        private System.Windows.Forms.Button QuanLyLop;


    }
}

