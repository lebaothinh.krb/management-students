﻿namespace QuanLiHocSinh
{
    partial class frmQuanLyLop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelbuttionsHocSinh = new System.Windows.Forms.Panel();
            this.btnHuyCapNhat = new System.Windows.Forms.Button();
            this.btnHuyThemLop = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnXoaTrang = new System.Windows.Forms.Button();
            this.btnCapNhat = new System.Windows.Forms.Button();
            this.dataGridViewLop = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSiso = new System.Windows.Forms.Label();
            this.lblLoaiKhoi = new System.Windows.Forms.Label();
            this.lblKhoiLop = new System.Windows.Forms.Label();
            this.lblGVCN = new System.Windows.Forms.Label();
            this.lblMaLop = new System.Windows.Forms.Label();
            this.txtSiSo = new System.Windows.Forms.TextBox();
            this.txtLoaiKhoi = new System.Windows.Forms.TextBox();
            this.txtKhoiLop = new System.Windows.Forms.TextBox();
            this.txtMaLop = new System.Windows.Forms.TextBox();
            this.txtGVCN = new System.Windows.Forms.TextBox();
            this.panelbuttionsHocSinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLop)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelbuttionsHocSinh
            // 
            this.panelbuttionsHocSinh.Controls.Add(this.btnHuyCapNhat);
            this.panelbuttionsHocSinh.Controls.Add(this.btnHuyThemLop);
            this.panelbuttionsHocSinh.Controls.Add(this.btnThem);
            this.panelbuttionsHocSinh.Controls.Add(this.btnXoa);
            this.panelbuttionsHocSinh.Controls.Add(this.btnXoaTrang);
            this.panelbuttionsHocSinh.Controls.Add(this.btnCapNhat);
            this.panelbuttionsHocSinh.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelbuttionsHocSinh.Location = new System.Drawing.Point(0, 0);
            this.panelbuttionsHocSinh.Name = "panelbuttionsHocSinh";
            this.panelbuttionsHocSinh.Size = new System.Drawing.Size(684, 112);
            this.panelbuttionsHocSinh.TabIndex = 10;
            // 
            // btnHuyCapNhat
            // 
            this.btnHuyCapNhat.Location = new System.Drawing.Point(564, 19);
            this.btnHuyCapNhat.Name = "btnHuyCapNhat";
            this.btnHuyCapNhat.Size = new System.Drawing.Size(97, 29);
            this.btnHuyCapNhat.TabIndex = 6;
            this.btnHuyCapNhat.Text = "Hủy Cập Nhật";
            this.btnHuyCapNhat.UseVisualStyleBackColor = true;
            this.btnHuyCapNhat.Visible = false;
            this.btnHuyCapNhat.Click += new System.EventHandler(this.btnHuyCapNhat_Click);
            // 
            // btnHuyThemLop
            // 
            this.btnHuyThemLop.Location = new System.Drawing.Point(564, 19);
            this.btnHuyThemLop.Name = "btnHuyThemLop";
            this.btnHuyThemLop.Size = new System.Drawing.Size(97, 28);
            this.btnHuyThemLop.TabIndex = 3;
            this.btnHuyThemLop.Text = "Hủy Thêm Lớp";
            this.btnHuyThemLop.UseVisualStyleBackColor = true;
            this.btnHuyThemLop.Visible = false;
            this.btnHuyThemLop.Click += new System.EventHandler(this.btnHuyThemLop_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(442, 18);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(97, 29);
            this.btnThem.TabIndex = 4;
            this.btnThem.Text = "Thêm Lớp";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(564, 19);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(97, 29);
            this.btnXoa.TabIndex = 0;
            this.btnXoa.Text = "Xóa Lớp";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnXoaTrang
            // 
            this.btnXoaTrang.Location = new System.Drawing.Point(564, 65);
            this.btnXoaTrang.Name = "btnXoaTrang";
            this.btnXoaTrang.Size = new System.Drawing.Size(97, 29);
            this.btnXoaTrang.TabIndex = 7;
            this.btnXoaTrang.Text = "Xóa Trắng";
            this.btnXoaTrang.UseVisualStyleBackColor = true;
            this.btnXoaTrang.Click += new System.EventHandler(this.btnXoaTrang_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(442, 65);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(97, 29);
            this.btnCapNhat.TabIndex = 5;
            this.btnCapNhat.Text = "Cập Nhật";
            this.btnCapNhat.UseVisualStyleBackColor = true;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // dataGridViewLop
            // 
            this.dataGridViewLop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLop.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewLop.Location = new System.Drawing.Point(0, 112);
            this.dataGridViewLop.Name = "dataGridViewLop";
            this.dataGridViewLop.Size = new System.Drawing.Size(684, 250);
            this.dataGridViewLop.TabIndex = 9;
            this.dataGridViewLop.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLop_RowEnter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblSiso);
            this.panel1.Controls.Add(this.lblLoaiKhoi);
            this.panel1.Controls.Add(this.lblKhoiLop);
            this.panel1.Controls.Add(this.lblGVCN);
            this.panel1.Controls.Add(this.lblMaLop);
            this.panel1.Controls.Add(this.txtSiSo);
            this.panel1.Controls.Add(this.txtLoaiKhoi);
            this.panel1.Controls.Add(this.txtKhoiLop);
            this.panel1.Controls.Add(this.txtMaLop);
            this.panel1.Controls.Add(this.txtGVCN);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(409, 112);
            this.panel1.TabIndex = 11;
            // 
            // lblSiso
            // 
            this.lblSiso.AutoSize = true;
            this.lblSiso.Location = new System.Drawing.Point(13, 81);
            this.lblSiso.Name = "lblSiso";
            this.lblSiso.Size = new System.Drawing.Size(32, 13);
            this.lblSiso.TabIndex = 1;
            this.lblSiso.Text = "Sỉ Số";
            // 
            // lblLoaiKhoi
            // 
            this.lblLoaiKhoi.AutoSize = true;
            this.lblLoaiKhoi.Location = new System.Drawing.Point(211, 51);
            this.lblLoaiKhoi.Name = "lblLoaiKhoi";
            this.lblLoaiKhoi.Size = new System.Drawing.Size(51, 13);
            this.lblLoaiKhoi.TabIndex = 1;
            this.lblLoaiKhoi.Text = "Loại Khối";
            // 
            // lblKhoiLop
            // 
            this.lblKhoiLop.AutoSize = true;
            this.lblKhoiLop.Location = new System.Drawing.Point(211, 19);
            this.lblKhoiLop.Name = "lblKhoiLop";
            this.lblKhoiLop.Size = new System.Drawing.Size(49, 13);
            this.lblKhoiLop.TabIndex = 1;
            this.lblKhoiLop.Text = "Khối Lớp";
            // 
            // lblGVCN
            // 
            this.lblGVCN.AutoSize = true;
            this.lblGVCN.Location = new System.Drawing.Point(13, 51);
            this.lblGVCN.Name = "lblGVCN";
            this.lblGVCN.Size = new System.Drawing.Size(37, 13);
            this.lblGVCN.TabIndex = 1;
            this.lblGVCN.Text = "GVCN";
            // 
            // lblMaLop
            // 
            this.lblMaLop.AutoSize = true;
            this.lblMaLop.Location = new System.Drawing.Point(13, 19);
            this.lblMaLop.Name = "lblMaLop";
            this.lblMaLop.Size = new System.Drawing.Size(43, 13);
            this.lblMaLop.TabIndex = 1;
            this.lblMaLop.Text = "Mã Lớp";
            // 
            // txtSiSo
            // 
            this.txtSiSo.Location = new System.Drawing.Point(88, 77);
            this.txtSiSo.Name = "txtSiSo";
            this.txtSiSo.ReadOnly = true;
            this.txtSiSo.Size = new System.Drawing.Size(100, 20);
            this.txtSiSo.TabIndex = 2;
            this.txtSiSo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSiSo_KeyPress);
            // 
            // txtLoaiKhoi
            // 
            this.txtLoaiKhoi.Location = new System.Drawing.Point(277, 47);
            this.txtLoaiKhoi.Name = "txtLoaiKhoi";
            this.txtLoaiKhoi.ReadOnly = true;
            this.txtLoaiKhoi.Size = new System.Drawing.Size(100, 20);
            this.txtLoaiKhoi.TabIndex = 0;
            // 
            // txtKhoiLop
            // 
            this.txtKhoiLop.Location = new System.Drawing.Point(277, 15);
            this.txtKhoiLop.Name = "txtKhoiLop";
            this.txtKhoiLop.ReadOnly = true;
            this.txtKhoiLop.Size = new System.Drawing.Size(100, 20);
            this.txtKhoiLop.TabIndex = 0;
            // 
            // txtMaLop
            // 
            this.txtMaLop.Location = new System.Drawing.Point(88, 15);
            this.txtMaLop.Name = "txtMaLop";
            this.txtMaLop.ReadOnly = true;
            this.txtMaLop.Size = new System.Drawing.Size(100, 20);
            this.txtMaLop.TabIndex = 0;
            // 
            // txtGVCN
            // 
            this.txtGVCN.Location = new System.Drawing.Point(88, 47);
            this.txtGVCN.Name = "txtGVCN";
            this.txtGVCN.ReadOnly = true;
            this.txtGVCN.Size = new System.Drawing.Size(100, 20);
            this.txtGVCN.TabIndex = 1;
            // 
            // frmQuanLyLop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelbuttionsHocSinh);
            this.Controls.Add(this.dataGridViewLop);
            this.MaximumSize = new System.Drawing.Size(700, 400);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "frmQuanLyLop";
            this.Text = "Quản Lý Lớp";
            this.Load += new System.EventHandler(this.frmQuanLyLop_Load);
            this.panelbuttionsHocSinh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLop)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelbuttionsHocSinh;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnCapNhat;
        private System.Windows.Forms.DataGridView dataGridViewLop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblSiso;
        private System.Windows.Forms.Label lblLoaiKhoi;
        private System.Windows.Forms.Label lblKhoiLop;
        private System.Windows.Forms.Label lblGVCN;
        private System.Windows.Forms.Label lblMaLop;
        private System.Windows.Forms.TextBox txtSiSo;
        private System.Windows.Forms.TextBox txtGVCN;
        private System.Windows.Forms.TextBox txtLoaiKhoi;
        private System.Windows.Forms.TextBox txtKhoiLop;
        private System.Windows.Forms.Button btnXoaTrang;
        private System.Windows.Forms.Button btnHuyThemLop;
        private System.Windows.Forms.Button btnHuyCapNhat;
        private System.Windows.Forms.TextBox txtMaLop;
    }
}