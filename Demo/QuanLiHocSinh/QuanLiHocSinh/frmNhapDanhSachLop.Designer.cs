﻿namespace QuanLiHocSinh
{
    partial class frmNhapDanhSachLop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSiso = new System.Windows.Forms.TextBox();
            this.lblLop = new System.Windows.Forms.Label();
            this.lblSiSo = new System.Windows.Forms.Label();
            this.cbLop = new System.Windows.Forms.ComboBox();
            this.pnIF = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbLoadSVCoLop = new System.Windows.Forms.GroupBox();
            this.dataGridViewCoLop = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gbLoadSVChuaLop = new System.Windows.Forms.GroupBox();
            this.dataGridViewChuaLop = new System.Windows.Forms.DataGridView();
            this.pnToolBox = new System.Windows.Forms.Panel();
            this.btnXoaKhoiLop = new System.Windows.Forms.Button();
            this.btnThemVaoLop = new System.Windows.Forms.Button();
            this.pnIF.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbLoadSVCoLop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoLop)).BeginInit();
            this.panel3.SuspendLayout();
            this.gbLoadSVChuaLop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChuaLop)).BeginInit();
            this.pnToolBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSiso
            // 
            this.txtSiso.Location = new System.Drawing.Point(235, 28);
            this.txtSiso.Name = "txtSiso";
            this.txtSiso.ReadOnly = true;
            this.txtSiso.Size = new System.Drawing.Size(76, 20);
            this.txtSiso.TabIndex = 2;
            // 
            // lblLop
            // 
            this.lblLop.AutoSize = true;
            this.lblLop.Location = new System.Drawing.Point(12, 31);
            this.lblLop.Name = "lblLop";
            this.lblLop.Size = new System.Drawing.Size(25, 13);
            this.lblLop.TabIndex = 3;
            this.lblLop.Text = "Lớp";
            // 
            // lblSiSo
            // 
            this.lblSiSo.AutoSize = true;
            this.lblSiSo.Location = new System.Drawing.Point(197, 31);
            this.lblSiSo.Name = "lblSiSo";
            this.lblSiSo.Size = new System.Drawing.Size(32, 13);
            this.lblSiSo.TabIndex = 4;
            this.lblSiSo.Text = "Sỉ Số";
            // 
            // cbLop
            // 
            this.cbLop.FormattingEnabled = true;
            this.cbLop.Location = new System.Drawing.Point(53, 28);
            this.cbLop.Name = "cbLop";
            this.cbLop.Size = new System.Drawing.Size(121, 21);
            this.cbLop.TabIndex = 0;
            this.cbLop.SelectedIndexChanged += new System.EventHandler(this.cbLop_SelectedIndexChanged);
            // 
            // pnIF
            // 
            this.pnIF.Controls.Add(this.txtSiso);
            this.pnIF.Controls.Add(this.cbLop);
            this.pnIF.Controls.Add(this.lblSiSo);
            this.pnIF.Controls.Add(this.lblLop);
            this.pnIF.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnIF.Location = new System.Drawing.Point(0, 0);
            this.pnIF.Name = "pnIF";
            this.pnIF.Size = new System.Drawing.Size(338, 78);
            this.pnIF.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbLoadSVCoLop);
            this.panel2.Controls.Add(this.pnIF);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(338, 362);
            this.panel2.TabIndex = 8;
            // 
            // gbLoadSVCoLop
            // 
            this.gbLoadSVCoLop.Controls.Add(this.dataGridViewCoLop);
            this.gbLoadSVCoLop.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbLoadSVCoLop.Location = new System.Drawing.Point(0, 77);
            this.gbLoadSVCoLop.Name = "gbLoadSVCoLop";
            this.gbLoadSVCoLop.Size = new System.Drawing.Size(338, 285);
            this.gbLoadSVCoLop.TabIndex = 7;
            this.gbLoadSVCoLop.TabStop = false;
            this.gbLoadSVCoLop.Text = "Sinh Viên Đã Có Lớp";
            // 
            // dataGridViewCoLop
            // 
            this.dataGridViewCoLop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCoLop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCoLop.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewCoLop.Name = "dataGridViewCoLop";
            this.dataGridViewCoLop.Size = new System.Drawing.Size(332, 266);
            this.dataGridViewCoLop.TabIndex = 0;
            this.dataGridViewCoLop.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCoLop_CellContentClick);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gbLoadSVChuaLop);
            this.panel3.Controls.Add(this.pnToolBox);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(344, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(340, 362);
            this.panel3.TabIndex = 9;
            // 
            // gbLoadSVChuaLop
            // 
            this.gbLoadSVChuaLop.Controls.Add(this.dataGridViewChuaLop);
            this.gbLoadSVChuaLop.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbLoadSVChuaLop.Location = new System.Drawing.Point(0, 0);
            this.gbLoadSVChuaLop.Name = "gbLoadSVChuaLop";
            this.gbLoadSVChuaLop.Size = new System.Drawing.Size(340, 287);
            this.gbLoadSVChuaLop.TabIndex = 7;
            this.gbLoadSVChuaLop.TabStop = false;
            this.gbLoadSVChuaLop.Text = "Sinh Viên Đã Chưa Có Lớp";
            // 
            // dataGridViewChuaLop
            // 
            this.dataGridViewChuaLop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewChuaLop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewChuaLop.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewChuaLop.Name = "dataGridViewChuaLop";
            this.dataGridViewChuaLop.Size = new System.Drawing.Size(334, 268);
            this.dataGridViewChuaLop.TabIndex = 0;
            // 
            // pnToolBox
            // 
            this.pnToolBox.Controls.Add(this.btnXoaKhoiLop);
            this.pnToolBox.Controls.Add(this.btnThemVaoLop);
            this.pnToolBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnToolBox.Location = new System.Drawing.Point(0, 284);
            this.pnToolBox.Name = "pnToolBox";
            this.pnToolBox.Size = new System.Drawing.Size(340, 78);
            this.pnToolBox.TabIndex = 7;
            // 
            // btnXoaKhoiLop
            // 
            this.btnXoaKhoiLop.Location = new System.Drawing.Point(177, 19);
            this.btnXoaKhoiLop.Name = "btnXoaKhoiLop";
            this.btnXoaKhoiLop.Size = new System.Drawing.Size(115, 39);
            this.btnXoaKhoiLop.TabIndex = 2;
            this.btnXoaKhoiLop.Text = "Xóa Khỏi Lớp Này";
            this.btnXoaKhoiLop.UseVisualStyleBackColor = true;
            this.btnXoaKhoiLop.Click += new System.EventHandler(this.btnXoaKhoiLop_Click);
            // 
            // btnThemVaoLop
            // 
            this.btnThemVaoLop.Location = new System.Drawing.Point(37, 19);
            this.btnThemVaoLop.Name = "btnThemVaoLop";
            this.btnThemVaoLop.Size = new System.Drawing.Size(115, 39);
            this.btnThemVaoLop.TabIndex = 1;
            this.btnThemVaoLop.Text = "Thêm Vào Lớp Này";
            this.btnThemVaoLop.UseVisualStyleBackColor = true;
            this.btnThemVaoLop.Click += new System.EventHandler(this.btnThemVaoLop_Click);
            // 
            // frmNhapDanhSachLop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "frmNhapDanhSachLop";
            this.Text = "Nhập Danh Sách Lớp";
            this.Load += new System.EventHandler(this.frmNhapDanhSachLop_Load);
            this.pnIF.ResumeLayout(false);
            this.pnIF.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbLoadSVCoLop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoLop)).EndInit();
            this.panel3.ResumeLayout(false);
            this.gbLoadSVChuaLop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChuaLop)).EndInit();
            this.pnToolBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtSiso;
        private System.Windows.Forms.Label lblLop;
        private System.Windows.Forms.Label lblSiSo;
        private System.Windows.Forms.ComboBox cbLop;
        private System.Windows.Forms.Panel pnIF;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbLoadSVCoLop;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox gbLoadSVChuaLop;
        private System.Windows.Forms.Panel pnToolBox;
        private System.Windows.Forms.Button btnXoaKhoiLop;
        private System.Windows.Forms.Button btnThemVaoLop;
        private System.Windows.Forms.DataGridView dataGridViewCoLop;
        private System.Windows.Forms.DataGridView dataGridViewChuaLop;
    }
}