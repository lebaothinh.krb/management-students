﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogic;
using System.Data.SqlClient;

namespace QuanLiHocSinh
{
    public partial class frmQuanLyMonHoc : Form
    {
        public frmQuanLyMonHoc()
        {
            InitializeComponent();
        }
        MonHoc mh = new MonHoc();
        private void frmQuanLyMonHoc_Load(object sender, EventArgs e)
        {
            Khoa();
            dataGridViewMonHoc.DataSource = mh.ShowMonHoc();
            dataGridViewMonHoc.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void MoKhoa()
        {
            txtMaMonHoc.ReadOnly = false;
            txtTenMonHoc.ReadOnly = false;
        }

        private void Khoa()
        {
            txtMaMonHoc.ReadOnly = true;
            txtTenMonHoc.ReadOnly = true;
        }
        bool Them = true, CapNhat = true;
        private void btnThem_Click(object sender, EventArgs e)
        {
            Them = !Them;
            if (Them == false)
            {
                btnXoaTrang_Click(null, null);
                dataGridViewMonHoc.Enabled = false;
                MoKhoa();
                btnCapNhat.Enabled = false;
                btnXoa.Enabled = false;
                btnHuyThem.Visible = true;
                btnThem.BackColor = Color.LightGray;
            }
            else
            {
                if (txtMaMonHoc.TextLength == 0)
                {
                    MessageBox.Show("Bạn Chưa Nhập Mã Môn Học");
                    Them = false;
                    return;
                }
                else if (txtMaMonHoc.TextLength > 4)
                {
                    MessageBox.Show("Mã Lớp Chỉ Chứa Tối Đa 4 Kí Tự");
                    Them = false;
                    return;
                }

                if (txtTenMonHoc.TextLength == 0)
                {
                    MessageBox.Show("Bạn Chưa Nhập Tên Môn Học");
                    Them = false;
                    return;
                }
                try
                {

                    mh.InsertMonHoc(this.txtMaMonHoc.Text, this.txtTenMonHoc.Text);
                    MessageBox.Show("Thêm Thành Công");
                    dataGridViewMonHoc.Enabled = true;
                    btnCapNhat.Enabled = true;
                    btnXoa.Enabled = true;
                    btnThem.BackColor = Color.Transparent;
                    btnHuyThem.Visible = false;
                    frmQuanLyMonHoc_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Lớp Đã Tồn Tại Rồi!");
                    Them = false;
                }
            } 
        }

        private void dataGridViewMonHoc_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int dong = e.RowIndex;
            try
            {
                txtMaMonHoc.Text = dataGridViewMonHoc.Rows[dong].Cells[0].Value.ToString();
                txtTenMonHoc.Text = dataGridViewMonHoc.Rows[dong].Cells[1].Value.ToString();
            }
            catch { }
        }
        string MaMonHocXoa = "";
        private void btnXoaTrang_Click(object sender, EventArgs e)
        {
            MaMonHocXoa = txtMaMonHoc.Text;
            txtMaMonHoc.Text = "";
            txtTenMonHoc.Text = "";
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            CapNhat = !CapNhat;
            string MaMonHocCu = MaMonHocXoa;
            if (txtMaMonHoc.Text != "")
                MaMonHocCu = txtMaMonHoc.Text;
            else txtMaMonHoc.Text = MaMonHocCu;
            if (CapNhat == false)
            {
                dataGridViewMonHoc.Enabled = false;
                btnThem.Enabled = false;
                btnXoa.Enabled = false;
                btnHuyCapNhat.Visible = true;
                btnCapNhat.BackColor = Color.LightGray;
                MoKhoa();
            }
            else
            {
                if (txtMaMonHoc.TextLength == 0)
                {
                    MessageBox.Show("Bạn Chưa Nhập Mã Môn Học");
                    CapNhat = false;
                    return;
                }
                else if (txtMaMonHoc.TextLength > 4)
                {
                    MessageBox.Show("Mã Môn Học Chỉ Chứa Tối Đa 4 Kí Tự");
                    CapNhat = false;
                    return;
                }

                if (txtTenMonHoc.TextLength == 0)
                {
                    MessageBox.Show("Bạn Chưa Nhập Tên Môn Học");
                    CapNhat = false;
                    return;
                }
                try
                {
                    mh.UpdateMonHoc(MaMonHocCu, this.txtMaMonHoc.Text, this.txtTenMonHoc.Text);
                    MessageBox.Show("Cập Nhật Thành Công");
                    dataGridViewMonHoc.Enabled = true;
                    Khoa();
                    btnThem.Enabled = true;
                    btnXoa.Enabled = true;
                    btnHuyCapNhat.Visible = false;
                    btnCapNhat.BackColor = Color.Transparent;
                    frmQuanLyMonHoc_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Lớp Đã Tồn Tại Rồi!");
                    CapNhat = false;
                }
            } 
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaMonHoc.Text != "")
                MaMonHocXoa = txtMaMonHoc.Text;
            DialogResult KQ = MessageBox.Show("Bạn Có Chắc Chắn Muốn Xóa Môn " + MaMonHocXoa + "?", "Quyết Định!", MessageBoxButtons.YesNo);
            if (KQ == DialogResult.Yes)
            {
                mh.DeleteMonHoc(MaMonHocXoa);
                MessageBox.Show("Xóa Thành Công!");
                frmQuanLyMonHoc_Load(null, e);
            }
        }

        private void btnHuyThem_Click(object sender, EventArgs e)
        {
            Them = true;
            dataGridViewMonHoc.Enabled = true;
            btnHuyThem.Visible = false;
            btnCapNhat.Enabled = true;
            btnXoa.Enabled = true;
            btnThem.BackColor = Color.Transparent;
            frmQuanLyMonHoc_Load(null, e);
        }

        private void btnHuyCapNhat_Click(object sender, EventArgs e)
        {
            CapNhat = true;
            dataGridViewMonHoc.Enabled = true;
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
            btnHuyCapNhat.Visible = false;
            btnCapNhat.BackColor = Color.Transparent;
            frmQuanLyMonHoc_Load(null, e);
        }
    }
}
