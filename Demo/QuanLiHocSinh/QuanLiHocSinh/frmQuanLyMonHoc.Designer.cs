﻿namespace QuanLiHocSinh
{
    partial class frmQuanLyMonHoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnHuyCapNhat = new System.Windows.Forms.Button();
            this.btnHuyThem = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnXoaTrang = new System.Windows.Forms.Button();
            this.btnCapNhat = new System.Windows.Forms.Button();
            this.dataGridViewMonHoc = new System.Windows.Forms.DataGridView();
            this.txtMaMonHoc = new System.Windows.Forms.TextBox();
            this.lblMaMonHoc = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtTenMonHoc = new System.Windows.Forms.TextBox();
            this.lblTenMonHoc = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMonHoc)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnHuyCapNhat);
            this.panel1.Controls.Add(this.btnHuyThem);
            this.panel1.Controls.Add(this.btnThem);
            this.panel1.Controls.Add(this.btnXoa);
            this.panel1.Controls.Add(this.btnXoaTrang);
            this.panel1.Controls.Add(this.btnCapNhat);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(253, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 100);
            this.panel1.TabIndex = 7;
            // 
            // btnHuyCapNhat
            // 
            this.btnHuyCapNhat.Location = new System.Drawing.Point(122, 11);
            this.btnHuyCapNhat.Name = "btnHuyCapNhat";
            this.btnHuyCapNhat.Size = new System.Drawing.Size(97, 27);
            this.btnHuyCapNhat.TabIndex = 4;
            this.btnHuyCapNhat.Text = "Hủy Cập Nhật";
            this.btnHuyCapNhat.UseVisualStyleBackColor = true;
            this.btnHuyCapNhat.Visible = false;
            this.btnHuyCapNhat.Click += new System.EventHandler(this.btnHuyCapNhat_Click);
            // 
            // btnHuyThem
            // 
            this.btnHuyThem.Location = new System.Drawing.Point(122, 10);
            this.btnHuyThem.Name = "btnHuyThem";
            this.btnHuyThem.Size = new System.Drawing.Size(97, 29);
            this.btnHuyThem.TabIndex = 3;
            this.btnHuyThem.Text = "Hủy Thêm";
            this.btnHuyThem.UseVisualStyleBackColor = true;
            this.btnHuyThem.Visible = false;
            this.btnHuyThem.Click += new System.EventHandler(this.btnHuyThem_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(15, 10);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(97, 29);
            this.btnThem.TabIndex = 2;
            this.btnThem.Text = "Thêm Môn Học";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(122, 10);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(97, 29);
            this.btnXoa.TabIndex = 0;
            this.btnXoa.Text = "Xóa Môn Học";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnXoaTrang
            // 
            this.btnXoaTrang.Location = new System.Drawing.Point(122, 55);
            this.btnXoaTrang.Name = "btnXoaTrang";
            this.btnXoaTrang.Size = new System.Drawing.Size(97, 29);
            this.btnXoaTrang.TabIndex = 4;
            this.btnXoaTrang.Text = "Xóa Trắng";
            this.btnXoaTrang.UseVisualStyleBackColor = true;
            this.btnXoaTrang.Click += new System.EventHandler(this.btnXoaTrang_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(15, 55);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(97, 29);
            this.btnCapNhat.TabIndex = 3;
            this.btnCapNhat.Text = "Cập Nhật";
            this.btnCapNhat.UseVisualStyleBackColor = true;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // dataGridViewMonHoc
            // 
            this.dataGridViewMonHoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMonHoc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewMonHoc.Location = new System.Drawing.Point(0, 100);
            this.dataGridViewMonHoc.Name = "dataGridViewMonHoc";
            this.dataGridViewMonHoc.Size = new System.Drawing.Size(484, 302);
            this.dataGridViewMonHoc.TabIndex = 6;
            this.dataGridViewMonHoc.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMonHoc_RowEnter);
            // 
            // txtMaMonHoc
            // 
            this.txtMaMonHoc.Location = new System.Drawing.Point(97, 17);
            this.txtMaMonHoc.Name = "txtMaMonHoc";
            this.txtMaMonHoc.Size = new System.Drawing.Size(133, 20);
            this.txtMaMonHoc.TabIndex = 0;
            // 
            // lblMaMonHoc
            // 
            this.lblMaMonHoc.AutoSize = true;
            this.lblMaMonHoc.Location = new System.Drawing.Point(17, 21);
            this.lblMaMonHoc.Name = "lblMaMonHoc";
            this.lblMaMonHoc.Size = new System.Drawing.Size(69, 13);
            this.lblMaMonHoc.TabIndex = 9;
            this.lblMaMonHoc.Text = "Mã Môn Học";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtTenMonHoc);
            this.panel2.Controls.Add(this.lblTenMonHoc);
            this.panel2.Controls.Add(this.txtMaMonHoc);
            this.panel2.Controls.Add(this.lblMaMonHoc);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(247, 100);
            this.panel2.TabIndex = 10;
            // 
            // txtTenMonHoc
            // 
            this.txtTenMonHoc.Location = new System.Drawing.Point(97, 54);
            this.txtTenMonHoc.Name = "txtTenMonHoc";
            this.txtTenMonHoc.Size = new System.Drawing.Size(133, 20);
            this.txtTenMonHoc.TabIndex = 1;
            // 
            // lblTenMonHoc
            // 
            this.lblTenMonHoc.AutoSize = true;
            this.lblTenMonHoc.Location = new System.Drawing.Point(17, 58);
            this.lblTenMonHoc.Name = "lblTenMonHoc";
            this.lblTenMonHoc.Size = new System.Drawing.Size(73, 13);
            this.lblTenMonHoc.TabIndex = 9;
            this.lblTenMonHoc.Text = "Tên Môn Học";
            // 
            // frmQuanLyMonHoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 402);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewMonHoc);
            this.MaximumSize = new System.Drawing.Size(500, 440);
            this.MinimumSize = new System.Drawing.Size(440, 400);
            this.Name = "frmQuanLyMonHoc";
            this.Text = "Quản Lý Môn Học";
            this.Load += new System.EventHandler(this.frmQuanLyMonHoc_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMonHoc)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnCapNhat;
        private System.Windows.Forms.DataGridView dataGridViewMonHoc;
        private System.Windows.Forms.TextBox txtMaMonHoc;
        private System.Windows.Forms.Label lblMaMonHoc;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtTenMonHoc;
        private System.Windows.Forms.Label lblTenMonHoc;
        private System.Windows.Forms.Button btnXoaTrang;
        private System.Windows.Forms.Button btnHuyThem;
        private System.Windows.Forms.Button btnHuyCapNhat;
    }
}