﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLogic;
using System.Data.SqlClient;

namespace QuanLiHocSinh
{
    public partial class frmQuanLyDiem : Form
    {
        public frmQuanLyDiem()
        {
            InitializeComponent();
        }
        Diem diem = new Diem();
        private bool checkdiem()
        {
            double diem15=0;
            double diem1t=0;
            double diemchk=0;
            try
            {
                diem15 = Convert.ToDouble(txtDiem15.Text);
                diem1t = Convert.ToDouble(txtDiem1T.Text);
                diemchk = Convert.ToDouble(txtDiemCHK.Text);
            }
            catch 
            {
                return false;
            }
            if ((diem15 < 0 || diem15 > 10) || (diem1t < 0 || diem1t > 10) || (diemchk < 0 || diemchk > 10)) return false;
            return true;
        }
        private void frmQuanLyDiem_Load(object sender, EventArgs e)
        {
            Khoa();
            cbMaHS.DataSource = diem.LoadMaHS();
            cbMaHS.DisplayMember = "MAHS";
            cbMaHS.ValueMember = "MAHS";
            cbMon.DataSource = diem.LoadMon();
            cbMon.DisplayMember = "MAMH";
            cbMon.ValueMember = "MAMH";
            dataGridViewDiem.DataSource = diem.ShowDiem();
            dataGridViewDiem.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridViewDiem_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int dong = e.RowIndex;
            try
            {
                cbMaHS.Text = dataGridViewDiem.Rows[dong].Cells[0].Value.ToString();
                cbHocKy.Text = dataGridViewDiem.Rows[dong].Cells[1].Value.ToString();
                cbMon.Text = dataGridViewDiem.Rows[dong].Cells[2].Value.ToString();
                txtDiem15.Text = dataGridViewDiem.Rows[dong].Cells[3].Value.ToString();
                txtDiem1T.Text = dataGridViewDiem.Rows[dong].Cells[4].Value.ToString();
                txtDiemCHK.Text = dataGridViewDiem.Rows[dong].Cells[5].Value.ToString();
            }
            catch { }
        }

        public void Khoa()
        {
            cbMaHS.Enabled = false;
            cbHocKy.Enabled = false;
            cbMon.Enabled = false;
            txtDiem15.ReadOnly = true;
            txtDiem1T.ReadOnly = true;
            txtDiemCHK.ReadOnly = true;
        }
        public void MoKhoa()
        {
            cbMaHS.Enabled = true;
            cbHocKy.Enabled = true;
            cbMon.Enabled = true;
            txtDiem15.ReadOnly = false;
            txtDiem1T.ReadOnly = false;
            txtDiemCHK.ReadOnly = false;
        }
        bool Them = true, CapNhat = true;
        public bool rangbuoc(ref bool CS)
        {
            if (cbMaHS.SelectedIndex < 0)
            {
                MessageBox.Show("Bạn Chưa Chọn Mã Học Sinh");
                CS = false;
                return false;
            }
            if (cbHocKy.SelectedIndex < 0)
            {
                MessageBox.Show("Bạn Chưa Chọn Học Kỳ");
                CS = false;
                return false;
            }
            if (cbMon.SelectedIndex < 0)
            {
                MessageBox.Show("Bạn Chưa Chọn Môn");
                CS = false;
                return false;
            }
            if (txtDiem15.TextLength == 0)
            {
                MessageBox.Show("Bạn Chưa Nhập Điểm 15 Phút");
                CS = false;
                return false;
            }
            if (txtDiem1T.TextLength == 0)
            {
                MessageBox.Show("Bạn Chưa Nhập Điểm 1 Tiết");
                CS = false;
                return false;
            }
            if (txtDiemCHK.TextLength == 0)
            {
                MessageBox.Show("Bạn Chưa Nhập Điểm Cuối Kỳ");
                CS = false;
                return false;
            }
            return true;
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            Them = !Them;
            if (Them == false)
            {
                btnXoaTrang_Click(null, null);
                dataGridViewDiem.Enabled = false;
                MoKhoa();
                btnCapNhat.Enabled = false;
                btnXoa.Enabled = false;
                btnHuyThem.Visible = true;
                btnThem.BackColor = Color.LightGray;
            }
            else
            {
                if (rangbuoc(ref Them) == false) return;
                if (checkdiem() == false)
                {
                    MessageBox.Show("Điểm phải từ 0 đến 10");
                    Them = false;
                    return;
                }
                try
                {
                    diem.InsertDiem(this.cbMaHS.Text, this.cbHocKy.Text, this.cbMon.Text, this.txtDiem15.Text, this.txtDiem1T.Text, this.txtDiemCHK.Text);
                    MessageBox.Show("Thêm Thành Công");
                    dataGridViewDiem.Enabled = true;
                    btnCapNhat.Enabled = true;
                    btnXoa.Enabled = true;
                    btnThem.BackColor = Color.Transparent;
                    btnHuyThem.Visible = false;
                    frmQuanLyDiem_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Bộ Này Đã Tồn Tại Rồi!");
                    Them = false;
                }
            }
        }
        string MaHSXoa = "", MaHKXoa = "", MaMonXoa = "";
        private void btnXoaTrang_Click(object sender, EventArgs e)
        {
            MaHSXoa = cbMaHS.Text;
            MaHKXoa = cbHocKy.Text;
            MaMonXoa = cbMon.Text;
            cbMaHS.SelectedIndex = -1;
            cbHocKy.SelectedIndex = -1;
            cbMon.SelectedIndex = -1;
            txtDiem15.Text = "";
            txtDiem1T.Text = "";
            txtDiemCHK.Text = "";
        }

        private void btnHuyThem_Click(object sender, EventArgs e)
        {
            Them = true;
            dataGridViewDiem.Enabled = true;
            btnHuyThem.Visible = false;
            btnCapNhat.Enabled = true;
            btnXoa.Enabled = true;
            btnThem.BackColor = Color.Transparent;
            frmQuanLyDiem_Load(null, e);
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            CapNhat = !CapNhat;
            string MaHSCu = MaHSXoa, HocKyCu = MaHKXoa, MonCu = MaMonXoa;
            if (cbMaHS.Text != "")
                MaHSCu = cbMaHS.Text;
            if (cbHocKy.Text != "")
                HocKyCu = cbHocKy.Text;
            if (cbMon.Text != "")
                MonCu = cbMon.Text;
            else cbMaHS.Text = MaHSCu;
            if (CapNhat == false)
            {
                dataGridViewDiem.Enabled = false;
                btnThem.Enabled = false;
                btnXoa.Enabled = false;
                btnHuyCapNhat.Visible = true;
                btnCapNhat.BackColor = Color.LightGray;
                MoKhoa();
            }
            else
            {
                if (rangbuoc(ref Them) == false) return;
                if (checkdiem() == false)
                {
                    MessageBox.Show("Điểm phải từ 0 đến 10");
                    CapNhat = false;
                    return;
                }
                try
                {
                    diem.UpdateDiem(MaHSCu, HocKyCu, MonCu, this.cbMaHS.Text, this.cbHocKy.Text, this.cbMon.Text, this.txtDiem15.Text, this.txtDiem1T.Text, this.txtDiemCHK.Text);
                    MessageBox.Show("Cập Nhật Thành Công");
                    dataGridViewDiem.Enabled = true;
                    Khoa();
                    btnThem.Enabled = true;
                    btnXoa.Enabled = true;
                    btnHuyCapNhat.Visible = false;
                    btnCapNhat.BackColor = Color.Transparent;
                    frmQuanLyDiem_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Bộ Này Đã Tồn Tại Rồi!");
                    CapNhat = false;
                }
            }
        }

        private void btnHuyCapNhat_Click(object sender, EventArgs e)
        {
            CapNhat = true;
            dataGridViewDiem.Enabled = true;
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
            btnHuyCapNhat.Visible = false;
            btnCapNhat.BackColor = Color.Transparent;
            frmQuanLyDiem_Load(null, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (cbMaHS.Text != "")
                MaHSXoa = cbMaHS.Text;
            if (cbHocKy.Text != "")
                MaHKXoa = cbHocKy.Text;
            if (cbMon.Text != "")
                MaMonXoa = cbMon.Text;
            DialogResult KQ = MessageBox.Show("Bạn Có Chắc Chắn Muốn Xóa Bộ ["+MaHSXoa+" - "+MaHKXoa+" - "+MaMonXoa+"]?", "Quyết Định!", MessageBoxButtons.YesNo);
            if (KQ == DialogResult.Yes)
            {
                try
                {
                    diem.DeleteDiem(MaHSXoa, MaHKXoa, MaMonXoa);
                    MessageBox.Show("Xóa Thành Công!");
                    frmQuanLyDiem_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Vui Lòng Chọn Bộ Để Xóa!");
                }
            }
        }
    }
}
