﻿namespace QuanLiHocSinh
{
    partial class frmNhanBangDiemMon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbIF = new System.Windows.Forms.GroupBox();
            this.cbHocKy = new System.Windows.Forms.ComboBox();
            this.cbMon = new System.Windows.Forms.ComboBox();
            this.cbLop = new System.Windows.Forms.ComboBox();
            this.btnTraCuu = new System.Windows.Forms.Button();
            this.lblHocKy = new System.Windows.Forms.Label();
            this.lblLop = new System.Windows.Forms.Label();
            this.lblMon = new System.Windows.Forms.Label();
            this.dataGridViewBangDiem = new System.Windows.Forms.DataGridView();
            this.gbIF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBangDiem)).BeginInit();
            this.SuspendLayout();
            // 
            // gbIF
            // 
            this.gbIF.Controls.Add(this.cbHocKy);
            this.gbIF.Controls.Add(this.cbMon);
            this.gbIF.Controls.Add(this.cbLop);
            this.gbIF.Controls.Add(this.btnTraCuu);
            this.gbIF.Controls.Add(this.lblHocKy);
            this.gbIF.Controls.Add(this.lblLop);
            this.gbIF.Controls.Add(this.lblMon);
            this.gbIF.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbIF.Location = new System.Drawing.Point(0, 0);
            this.gbIF.Name = "gbIF";
            this.gbIF.Size = new System.Drawing.Size(684, 89);
            this.gbIF.TabIndex = 11;
            this.gbIF.TabStop = false;
            this.gbIF.Text = "Thông Tin Tra Cứu";
            // 
            // cbHocKy
            // 
            this.cbHocKy.FormattingEnabled = true;
            this.cbHocKy.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cbHocKy.Location = new System.Drawing.Point(361, 19);
            this.cbHocKy.Name = "cbHocKy";
            this.cbHocKy.Size = new System.Drawing.Size(81, 21);
            this.cbHocKy.TabIndex = 2;
            // 
            // cbMon
            // 
            this.cbMon.FormattingEnabled = true;
            this.cbMon.Location = new System.Drawing.Point(49, 19);
            this.cbMon.Name = "cbMon";
            this.cbMon.Size = new System.Drawing.Size(81, 21);
            this.cbMon.TabIndex = 0;
            // 
            // cbLop
            // 
            this.cbLop.FormattingEnabled = true;
            this.cbLop.Location = new System.Drawing.Point(204, 19);
            this.cbLop.Name = "cbLop";
            this.cbLop.Size = new System.Drawing.Size(81, 21);
            this.cbLop.TabIndex = 1;
            // 
            // btnTraCuu
            // 
            this.btnTraCuu.Location = new System.Drawing.Point(299, 55);
            this.btnTraCuu.Name = "btnTraCuu";
            this.btnTraCuu.Size = new System.Drawing.Size(75, 23);
            this.btnTraCuu.TabIndex = 3;
            this.btnTraCuu.Text = "Tra Cứu";
            this.btnTraCuu.UseVisualStyleBackColor = true;
            this.btnTraCuu.Click += new System.EventHandler(this.btnTraCuu_Click);
            // 
            // lblHocKy
            // 
            this.lblHocKy.AutoSize = true;
            this.lblHocKy.Location = new System.Drawing.Point(313, 24);
            this.lblHocKy.Name = "lblHocKy";
            this.lblHocKy.Size = new System.Drawing.Size(42, 13);
            this.lblHocKy.TabIndex = 3;
            this.lblHocKy.Text = "Học Kỳ";
            // 
            // lblLop
            // 
            this.lblLop.AutoSize = true;
            this.lblLop.Location = new System.Drawing.Point(177, 24);
            this.lblLop.Name = "lblLop";
            this.lblLop.Size = new System.Drawing.Size(25, 13);
            this.lblLop.TabIndex = 2;
            this.lblLop.Text = "Lớp";
            // 
            // lblMon
            // 
            this.lblMon.AutoSize = true;
            this.lblMon.Location = new System.Drawing.Point(15, 24);
            this.lblMon.Name = "lblMon";
            this.lblMon.Size = new System.Drawing.Size(28, 13);
            this.lblMon.TabIndex = 1;
            this.lblMon.Text = "Môn";
            // 
            // dataGridViewBangDiem
            // 
            this.dataGridViewBangDiem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBangDiem.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewBangDiem.Location = new System.Drawing.Point(0, 95);
            this.dataGridViewBangDiem.Name = "dataGridViewBangDiem";
            this.dataGridViewBangDiem.Size = new System.Drawing.Size(684, 267);
            this.dataGridViewBangDiem.TabIndex = 4;
            // 
            // frmNhanBangDiemMon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.gbIF);
            this.Controls.Add(this.dataGridViewBangDiem);
            this.MaximumSize = new System.Drawing.Size(700, 400);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "frmNhanBangDiemMon";
            this.Text = "Nhập Bảng Điểm Môn";
            this.Load += new System.EventHandler(this.frmNhanBangDiemMon_Load);
            this.gbIF.ResumeLayout(false);
            this.gbIF.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBangDiem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbIF;
        private System.Windows.Forms.Button btnTraCuu;
        private System.Windows.Forms.Label lblHocKy;
        private System.Windows.Forms.Label lblLop;
        private System.Windows.Forms.Label lblMon;
        private System.Windows.Forms.DataGridView dataGridViewBangDiem;
        private System.Windows.Forms.ComboBox cbLop;
        private System.Windows.Forms.ComboBox cbHocKy;
        private System.Windows.Forms.ComboBox cbMon;
    }
}