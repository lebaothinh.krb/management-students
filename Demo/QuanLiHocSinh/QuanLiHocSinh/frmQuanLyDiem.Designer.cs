﻿namespace QuanLiHocSinh
{
    partial class frmQuanLyDiem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewDiem = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbMon = new System.Windows.Forms.ComboBox();
            this.cbHocKy = new System.Windows.Forms.ComboBox();
            this.cbMaHS = new System.Windows.Forms.ComboBox();
            this.lblDiem15 = new System.Windows.Forms.Label();
            this.lblMon = new System.Windows.Forms.Label();
            this.lblDiemCHK = new System.Windows.Forms.Label();
            this.lblDiem1T = new System.Windows.Forms.Label();
            this.lblHocKy = new System.Windows.Forms.Label();
            this.lblMaHS = new System.Windows.Forms.Label();
            this.txtDiem15 = new System.Windows.Forms.TextBox();
            this.txtDiem1T = new System.Windows.Forms.TextBox();
            this.txtDiemCHK = new System.Windows.Forms.TextBox();
            this.panelbuttionsHocSinh = new System.Windows.Forms.Panel();
            this.btnHuyCapNhat = new System.Windows.Forms.Button();
            this.btnHuyThem = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnXoaTrang = new System.Windows.Forms.Button();
            this.btnCapNhat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDiem)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelbuttionsHocSinh.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewDiem
            // 
            this.dataGridViewDiem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDiem.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewDiem.Location = new System.Drawing.Point(0, 127);
            this.dataGridViewDiem.Name = "dataGridViewDiem";
            this.dataGridViewDiem.Size = new System.Drawing.Size(684, 235);
            this.dataGridViewDiem.TabIndex = 7;
            this.dataGridViewDiem.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDiem_RowEnter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbMon);
            this.panel1.Controls.Add(this.cbHocKy);
            this.panel1.Controls.Add(this.cbMaHS);
            this.panel1.Controls.Add(this.lblDiem15);
            this.panel1.Controls.Add(this.lblMon);
            this.panel1.Controls.Add(this.lblDiemCHK);
            this.panel1.Controls.Add(this.lblDiem1T);
            this.panel1.Controls.Add(this.lblHocKy);
            this.panel1.Controls.Add(this.lblMaHS);
            this.panel1.Controls.Add(this.txtDiem15);
            this.panel1.Controls.Add(this.txtDiem1T);
            this.panel1.Controls.Add(this.txtDiemCHK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 127);
            this.panel1.TabIndex = 9;
            // 
            // cbMon
            // 
            this.cbMon.FormattingEnabled = true;
            this.cbMon.Location = new System.Drawing.Point(89, 91);
            this.cbMon.Name = "cbMon";
            this.cbMon.Size = new System.Drawing.Size(90, 21);
            this.cbMon.TabIndex = 2;
            // 
            // cbHocKy
            // 
            this.cbHocKy.FormattingEnabled = true;
            this.cbHocKy.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cbHocKy.Location = new System.Drawing.Point(89, 53);
            this.cbHocKy.Name = "cbHocKy";
            this.cbHocKy.Size = new System.Drawing.Size(90, 21);
            this.cbHocKy.TabIndex = 1;
            // 
            // cbMaHS
            // 
            this.cbMaHS.FormattingEnabled = true;
            this.cbMaHS.Location = new System.Drawing.Point(89, 15);
            this.cbMaHS.Name = "cbMaHS";
            this.cbMaHS.Size = new System.Drawing.Size(90, 21);
            this.cbMaHS.TabIndex = 0;
            // 
            // lblDiem15
            // 
            this.lblDiem15.AutoSize = true;
            this.lblDiem15.Location = new System.Drawing.Point(205, 19);
            this.lblDiem15.Name = "lblDiem15";
            this.lblDiem15.Size = new System.Drawing.Size(71, 13);
            this.lblDiem15.TabIndex = 1;
            this.lblDiem15.Text = "Điểm 15 Phút";
            // 
            // lblMon
            // 
            this.lblMon.AutoSize = true;
            this.lblMon.Location = new System.Drawing.Point(13, 95);
            this.lblMon.Name = "lblMon";
            this.lblMon.Size = new System.Drawing.Size(28, 13);
            this.lblMon.TabIndex = 1;
            this.lblMon.Text = "Môn";
            // 
            // lblDiemCHK
            // 
            this.lblDiemCHK.AutoSize = true;
            this.lblDiemCHK.Location = new System.Drawing.Point(205, 95);
            this.lblDiemCHK.Name = "lblDiemCHK";
            this.lblDiemCHK.Size = new System.Drawing.Size(70, 13);
            this.lblDiemCHK.TabIndex = 1;
            this.lblDiemCHK.Text = "Điểm Cuối Kỳ";
            // 
            // lblDiem1T
            // 
            this.lblDiem1T.AutoSize = true;
            this.lblDiem1T.Location = new System.Drawing.Point(205, 57);
            this.lblDiem1T.Name = "lblDiem1T";
            this.lblDiem1T.Size = new System.Drawing.Size(61, 13);
            this.lblDiem1T.TabIndex = 1;
            this.lblDiem1T.Text = "Điểm 1 Tiết";
            // 
            // lblHocKy
            // 
            this.lblHocKy.AutoSize = true;
            this.lblHocKy.Location = new System.Drawing.Point(13, 57);
            this.lblHocKy.Name = "lblHocKy";
            this.lblHocKy.Size = new System.Drawing.Size(42, 13);
            this.lblHocKy.TabIndex = 1;
            this.lblHocKy.Text = "Học Kỳ";
            // 
            // lblMaHS
            // 
            this.lblMaHS.AutoSize = true;
            this.lblMaHS.Location = new System.Drawing.Point(13, 19);
            this.lblMaHS.Name = "lblMaHS";
            this.lblMaHS.Size = new System.Drawing.Size(69, 13);
            this.lblMaHS.TabIndex = 1;
            this.lblMaHS.Text = "Mã Học Sinh";
            // 
            // txtDiem15
            // 
            this.txtDiem15.Location = new System.Drawing.Point(282, 15);
            this.txtDiem15.Name = "txtDiem15";
            this.txtDiem15.Size = new System.Drawing.Size(89, 20);
            this.txtDiem15.TabIndex = 3;
            // 
            // txtDiem1T
            // 
            this.txtDiem1T.Location = new System.Drawing.Point(282, 53);
            this.txtDiem1T.Name = "txtDiem1T";
            this.txtDiem1T.Size = new System.Drawing.Size(89, 20);
            this.txtDiem1T.TabIndex = 4;
            // 
            // txtDiemCHK
            // 
            this.txtDiemCHK.Location = new System.Drawing.Point(282, 91);
            this.txtDiemCHK.Name = "txtDiemCHK";
            this.txtDiemCHK.Size = new System.Drawing.Size(89, 20);
            this.txtDiemCHK.TabIndex = 5;
            // 
            // panelbuttionsHocSinh
            // 
            this.panelbuttionsHocSinh.Controls.Add(this.btnHuyCapNhat);
            this.panelbuttionsHocSinh.Controls.Add(this.btnHuyThem);
            this.panelbuttionsHocSinh.Controls.Add(this.btnThem);
            this.panelbuttionsHocSinh.Controls.Add(this.btnXoa);
            this.panelbuttionsHocSinh.Controls.Add(this.btnXoaTrang);
            this.panelbuttionsHocSinh.Controls.Add(this.btnCapNhat);
            this.panelbuttionsHocSinh.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelbuttionsHocSinh.Location = new System.Drawing.Point(407, 0);
            this.panelbuttionsHocSinh.Name = "panelbuttionsHocSinh";
            this.panelbuttionsHocSinh.Size = new System.Drawing.Size(277, 127);
            this.panelbuttionsHocSinh.TabIndex = 8;
            // 
            // btnHuyCapNhat
            // 
            this.btnHuyCapNhat.Location = new System.Drawing.Point(149, 11);
            this.btnHuyCapNhat.Name = "btnHuyCapNhat";
            this.btnHuyCapNhat.Size = new System.Drawing.Size(97, 41);
            this.btnHuyCapNhat.TabIndex = 4;
            this.btnHuyCapNhat.Text = "Hủy Cập Nhật";
            this.btnHuyCapNhat.UseVisualStyleBackColor = true;
            this.btnHuyCapNhat.Visible = false;
            this.btnHuyCapNhat.Click += new System.EventHandler(this.btnHuyCapNhat_Click);
            // 
            // btnHuyThem
            // 
            this.btnHuyThem.Location = new System.Drawing.Point(149, 11);
            this.btnHuyThem.Name = "btnHuyThem";
            this.btnHuyThem.Size = new System.Drawing.Size(97, 41);
            this.btnHuyThem.TabIndex = 3;
            this.btnHuyThem.Text = "Hủy Thêm ";
            this.btnHuyThem.UseVisualStyleBackColor = true;
            this.btnHuyThem.Visible = false;
            this.btnHuyThem.Click += new System.EventHandler(this.btnHuyThem_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(32, 11);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(97, 41);
            this.btnThem.TabIndex = 6;
            this.btnThem.Text = "Thêm Điểm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(149, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(97, 41);
            this.btnXoa.TabIndex = 0;
            this.btnXoa.Text = "Xóa Điểm";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnXoaTrang
            // 
            this.btnXoaTrang.Location = new System.Drawing.Point(149, 69);
            this.btnXoaTrang.Name = "btnXoaTrang";
            this.btnXoaTrang.Size = new System.Drawing.Size(97, 41);
            this.btnXoaTrang.TabIndex = 8;
            this.btnXoaTrang.Text = "Xóa Trắng";
            this.btnXoaTrang.UseVisualStyleBackColor = true;
            this.btnXoaTrang.Click += new System.EventHandler(this.btnXoaTrang_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(32, 69);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(97, 41);
            this.btnCapNhat.TabIndex = 7;
            this.btnCapNhat.Text = "Cập Nhật Điểm";
            this.btnCapNhat.UseVisualStyleBackColor = true;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // frmQuanLyDiem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelbuttionsHocSinh);
            this.Controls.Add(this.dataGridViewDiem);
            this.MaximumSize = new System.Drawing.Size(700, 400);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "frmQuanLyDiem";
            this.Text = "Quản Lý Điểm";
            this.Load += new System.EventHandler(this.frmQuanLyDiem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDiem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelbuttionsHocSinh.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDiem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDiem15;
        private System.Windows.Forms.Label lblMon;
        private System.Windows.Forms.Label lblDiemCHK;
        private System.Windows.Forms.Label lblDiem1T;
        private System.Windows.Forms.Label lblHocKy;
        private System.Windows.Forms.Label lblMaHS;
        private System.Windows.Forms.TextBox txtDiem15;
        private System.Windows.Forms.TextBox txtDiemCHK;
        private System.Windows.Forms.Panel panelbuttionsHocSinh;
        private System.Windows.Forms.Button btnHuyCapNhat;
        private System.Windows.Forms.Button btnHuyThem;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnXoaTrang;
        private System.Windows.Forms.Button btnCapNhat;
        private System.Windows.Forms.TextBox txtDiem1T;
        private System.Windows.Forms.ComboBox cbMon;
        private System.Windows.Forms.ComboBox cbHocKy;
        private System.Windows.Forms.ComboBox cbMaHS;
    }
}