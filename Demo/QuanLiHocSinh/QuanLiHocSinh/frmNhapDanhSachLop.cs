﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BusinessLogic;

namespace QuanLiHocSinh
{
    public partial class frmNhapDanhSachLop : Form
    {
        public frmNhapDanhSachLop()
        {
            InitializeComponent();
        }
        Lop loph = new Lop();
        HocSinh hs = new HocSinh();
        Nhapdslop nhapdslop = new Nhapdslop();
        private void frmNhapDanhSachLop_Load(object sender, EventArgs e)
        {
            LoadMaLop();
            LoadSiSo();
            LoadSVChuaLop();
            LoadSVCoLop();
        }
        public void LoadMaLop()
        {
            cbLop.DataSource = loph.LoadMaLop();
            cbLop.DisplayMember = "TENLOP";
            cbLop.ValueMember = "TENLOP";
        }
        public void LoadSiSo()
        {
            DataRow siso = loph.LoadSiSo(cbLop.SelectedValue.ToString()).Rows[0];
            txtSiso.Text = siso["SISO"].ToString();
        }
        public void LoadSVChuaLop()
        {
            dataGridViewChuaLop.DataSource = nhapdslop.ShowSinhVienChuaCoLop();
        }
        public void LoadSVCoLop()
        {
            dataGridViewCoLop.DataSource = nhapdslop.ShowSinhVienDaCoLop(cbLop.SelectedValue.ToString());
        }
        private void cbLop_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSVCoLop();
            try
            {
                LoadSiSo();
            }
            catch { }
        }

        private void btnThemVaoLop_Click(object sender, EventArgs e)
        {
            try
            {
                int lop = Convert.ToInt32(cbLop.SelectedValue.ToString().Substring(0, 2)), tuoi = 0;
                string[] ns = new string[3];
                ns = dataGridViewChuaLop.CurrentRow.Cells[3].Value.ToString().Split('/');
                tuoi = DateTime.Now.Year - Convert.ToInt32(ns[2].Substring(0, 4));
                if (tuoi < 18 && lop == 12)
                {
                    MessageBox.Show("Lớp 12 phải từ 18 tuổi trở lên!");
                    return;
                }
                if (tuoi > 17 && lop == 10)
                {
                    MessageBox.Show("Lớp 10 phải dưới 18 tuổi");
                    return;
                }
                hs.UpdateLopChoHS(dataGridViewChuaLop.CurrentRow.Cells[0].Value.ToString(), cbLop.SelectedValue.ToString());
            }
            catch {
                MessageBox.Show("Hết Sinh Viên Chưa Có Lớp");
                return;
            }
            LoadSVChuaLop();
            LoadSVCoLop();
            LoadSiSo();
        }

        private void btnXoaKhoiLop_Click(object sender, EventArgs e)
        {
            try
            {
                hs.UpdateLopChoHS(dataGridViewCoLop.CurrentRow.Cells[0].Value.ToString(), "NULL");
            }
            catch { }
            LoadSVChuaLop();
            LoadSVCoLop();
            LoadSiSo();
        }

        private void dataGridViewCoLop_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
