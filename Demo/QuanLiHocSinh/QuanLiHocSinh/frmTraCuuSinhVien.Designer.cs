﻿namespace QuanLiHocSinh
{
    partial class frmTraCuuSinhVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewTracuu = new System.Windows.Forms.DataGridView();
            this.lblMaHocSinh = new System.Windows.Forms.Label();
            this.lblLop = new System.Windows.Forms.Label();
            this.gbIF = new System.Windows.Forms.GroupBox();
            this.cbTenHocSinh = new System.Windows.Forms.ComboBox();
            this.cbLop = new System.Windows.Forms.ComboBox();
            this.cbTracuu = new System.Windows.Forms.ComboBox();
            this.cbMaHS = new System.Windows.Forms.ComboBox();
            this.btnTraCuu = new System.Windows.Forms.Button();
            this.lblTenHocSinh = new System.Windows.Forms.Label();
            this.lblTracuu = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTracuu)).BeginInit();
            this.gbIF.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewTracuu
            // 
            this.dataGridViewTracuu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTracuu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridViewTracuu.Location = new System.Drawing.Point(0, 95);
            this.dataGridViewTracuu.Name = "dataGridViewTracuu";
            this.dataGridViewTracuu.Size = new System.Drawing.Size(684, 267);
            this.dataGridViewTracuu.TabIndex = 0;
            // 
            // lblMaHocSinh
            // 
            this.lblMaHocSinh.AutoSize = true;
            this.lblMaHocSinh.Location = new System.Drawing.Point(226, 25);
            this.lblMaHocSinh.Name = "lblMaHocSinh";
            this.lblMaHocSinh.Size = new System.Drawing.Size(69, 13);
            this.lblMaHocSinh.TabIndex = 1;
            this.lblMaHocSinh.Text = "Mã Học Sinh";
            // 
            // lblLop
            // 
            this.lblLop.AutoSize = true;
            this.lblLop.Location = new System.Drawing.Point(256, 25);
            this.lblLop.Name = "lblLop";
            this.lblLop.Size = new System.Drawing.Size(25, 13);
            this.lblLop.TabIndex = 2;
            this.lblLop.Text = "Lớp";
            // 
            // gbIF
            // 
            this.gbIF.Controls.Add(this.cbTenHocSinh);
            this.gbIF.Controls.Add(this.cbLop);
            this.gbIF.Controls.Add(this.cbTracuu);
            this.gbIF.Controls.Add(this.cbMaHS);
            this.gbIF.Controls.Add(this.btnTraCuu);
            this.gbIF.Controls.Add(this.lblTenHocSinh);
            this.gbIF.Controls.Add(this.lblTracuu);
            this.gbIF.Controls.Add(this.lblLop);
            this.gbIF.Controls.Add(this.lblMaHocSinh);
            this.gbIF.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbIF.Location = new System.Drawing.Point(0, 0);
            this.gbIF.Name = "gbIF";
            this.gbIF.Size = new System.Drawing.Size(684, 89);
            this.gbIF.TabIndex = 9;
            this.gbIF.TabStop = false;
            this.gbIF.Text = "Thông Tin Tra Cứu";
            // 
            // cbTenHocSinh
            // 
            this.cbTenHocSinh.FormattingEnabled = true;
            this.cbTenHocSinh.Location = new System.Drawing.Point(299, 21);
            this.cbTenHocSinh.Name = "cbTenHocSinh";
            this.cbTenHocSinh.Size = new System.Drawing.Size(121, 21);
            this.cbTenHocSinh.TabIndex = 1;
            // 
            // cbLop
            // 
            this.cbLop.FormattingEnabled = true;
            this.cbLop.Location = new System.Drawing.Point(299, 21);
            this.cbLop.Name = "cbLop";
            this.cbLop.Size = new System.Drawing.Size(121, 21);
            this.cbLop.TabIndex = 10;
            // 
            // cbTracuu
            // 
            this.cbTracuu.FormattingEnabled = true;
            this.cbTracuu.Items.AddRange(new object[] {
            "Mã Học Sinh",
            "Tên Học Sinh",
            "Lớp"});
            this.cbTracuu.Location = new System.Drawing.Point(87, 21);
            this.cbTracuu.Name = "cbTracuu";
            this.cbTracuu.Size = new System.Drawing.Size(121, 21);
            this.cbTracuu.TabIndex = 0;
            this.cbTracuu.SelectedIndexChanged += new System.EventHandler(this.cbTracuu_SelectedIndexChanged);
            // 
            // cbMaHS
            // 
            this.cbMaHS.FormattingEnabled = true;
            this.cbMaHS.Location = new System.Drawing.Point(299, 21);
            this.cbMaHS.Name = "cbMaHS";
            this.cbMaHS.Size = new System.Drawing.Size(121, 21);
            this.cbMaHS.TabIndex = 10;
            // 
            // btnTraCuu
            // 
            this.btnTraCuu.Location = new System.Drawing.Point(299, 56);
            this.btnTraCuu.Name = "btnTraCuu";
            this.btnTraCuu.Size = new System.Drawing.Size(75, 23);
            this.btnTraCuu.TabIndex = 2;
            this.btnTraCuu.Text = "Tra Cứu";
            this.btnTraCuu.UseVisualStyleBackColor = true;
            this.btnTraCuu.Click += new System.EventHandler(this.btnTraCuu_Click);
            // 
            // lblTenHocSinh
            // 
            this.lblTenHocSinh.AutoSize = true;
            this.lblTenHocSinh.Location = new System.Drawing.Point(226, 25);
            this.lblTenHocSinh.Name = "lblTenHocSinh";
            this.lblTenHocSinh.Size = new System.Drawing.Size(73, 13);
            this.lblTenHocSinh.TabIndex = 2;
            this.lblTenHocSinh.Text = "Tên Học Sinh";
            // 
            // lblTracuu
            // 
            this.lblTracuu.AutoSize = true;
            this.lblTracuu.Location = new System.Drawing.Point(14, 26);
            this.lblTracuu.Name = "lblTracuu";
            this.lblTracuu.Size = new System.Drawing.Size(73, 13);
            this.lblTracuu.TabIndex = 1;
            this.lblTracuu.Text = "Tra Cứu Theo";
            // 
            // frmTraCuuSinhVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 362);
            this.Controls.Add(this.gbIF);
            this.Controls.Add(this.dataGridViewTracuu);
            this.MaximumSize = new System.Drawing.Size(700, 400);
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "frmTraCuuSinhVien";
            this.Text = "Tra Cứu Sinh Viên";
            this.Load += new System.EventHandler(this.frmTraCuuSinhVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTracuu)).EndInit();
            this.gbIF.ResumeLayout(false);
            this.gbIF.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewTracuu;
        private System.Windows.Forms.Label lblMaHocSinh;
        private System.Windows.Forms.Label lblLop;
        private System.Windows.Forms.GroupBox gbIF;
        private System.Windows.Forms.Button btnTraCuu;
        private System.Windows.Forms.ComboBox cbLop;
        private System.Windows.Forms.ComboBox cbMaHS;
        private System.Windows.Forms.ComboBox cbTracuu;
        private System.Windows.Forms.Label lblTracuu;
        private System.Windows.Forms.ComboBox cbTenHocSinh;
        private System.Windows.Forms.Label lblTenHocSinh;
    }
}