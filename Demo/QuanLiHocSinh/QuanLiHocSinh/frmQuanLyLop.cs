﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BusinessLogic;
using DataAccess;
namespace QuanLiHocSinh
{
    public partial class frmQuanLyLop : Form
    {
        public frmQuanLyLop()
        {
            InitializeComponent();
        }
        Lop loph = new Lop();
        HocSinh hs = new HocSinh();
        private void frmQuanLyLop_Load(object sender, EventArgs e)
        {
            dataGridViewLop.DataSource = loph.ShowLop(); ;
            dataGridViewLop.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void dataGridViewLop_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int dong = e.RowIndex;
                this.txtMaLop.Text = dataGridViewLop.Rows[dong].Cells[0].Value.ToString();
                this.txtGVCN.Text = dataGridViewLop.Rows[dong].Cells[1].Value.ToString();
                this.txtSiSo.Text = dataGridViewLop.Rows[dong].Cells[2].Value.ToString();
                this.txtKhoiLop.Text = dataGridViewLop.Rows[dong].Cells[3].Value.ToString();
                this.txtLoaiKhoi.Text = dataGridViewLop.Rows[dong].Cells[4].Value.ToString();
            }
            catch { }
        }

        public void MoKhoa()
        {
            txtMaLop.ReadOnly = false;
            txtGVCN.ReadOnly = false;
            txtSiSo.ReadOnly = false;
        }

        public void Khoa()
        {
            txtMaLop.ReadOnly = true;
            txtGVCN.ReadOnly = true;
            txtSiSo.ReadOnly = true;
        }
        bool Them = true,CapNhat=true;
        private void btnThem_Click(object sender, EventArgs e)
        {
            Them = !Them;
            if (Them == false)
            {
                btnXoaTrang_Click(null, null);
                dataGridViewLop.Enabled = false;
                MoKhoa();
                btnCapNhat.Enabled = false;
                btnXoa.Enabled = false;
                btnHuyThemLop.Visible = true;
                btnThem.BackColor = Color.LightGray;
            }
            else
            {
                if (Convert.ToInt32(txtSiSo.Text) > 20)
                {
                    MessageBox.Show("Sỉ Số Vượt Quá Qui Định!");
                    Them = false;
                    return;
                }

                if (txtGVCN.TextLength == 0)
                {
                    MessageBox.Show("Bạn Chưa Nhập GVCN");
                    Them = false;
                    return;
                }
                try
                {
                    int lop=-1;
                    txtKhoiLop.Text = txtMaLop.Text.Substring(0, 2);
                    try
                    {
                        lop = Convert.ToInt32(txtKhoiLop.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Bạn Không Thể Nhập Lớp Này\nChỉ Gồm Các Khối 10, 11 và 12!");
                        Them = false;
                        return;
                    }
                    
                    if (lop!=11 && lop!=10 && lop!=12)
                    {
                        MessageBox.Show("Bạn Không Thể Nhập Lớp Này\nChỉ Gồm Các Khối 10, 11 và 12!");
                        Them = false;
                        return;
                    }
                    txtLoaiKhoi.Text = txtMaLop.Text.Substring(2, 1);
                    int loaikhoi=0;
                    try
                    {
                        loaikhoi = Convert.ToInt32(Convert.ToChar(txtLoaiKhoi.Text));
                    }
                    catch
                    {
                        MessageBox.Show("Loại Khối Phải Từ A->Z!");
                        Them = false;
                        return;
                    }
                    if (loaikhoi<65 || (loaikhoi>90 && loaikhoi<97) || loaikhoi>122 )
                    {
                        MessageBox.Show("Loại Khối Phải Từ A->Z!");
                        Them = false;
                        return;
                    }
                    loph.InsertLop(this.txtMaLop.Text, this.txtGVCN.Text, this.txtSiSo.Text, this.txtKhoiLop.Text, this.txtLoaiKhoi.Text);
                    MessageBox.Show("Thêm Thành Công");
                    dataGridViewLop.Enabled = true;
                    btnCapNhat.Enabled = true;
                    btnXoa.Enabled = true;
                    btnThem.BackColor = Color.Transparent;
                    btnHuyThemLop.Visible = false;
                    frmQuanLyLop_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Lớp Đã Tồn Tại Rồi!");
                    Them = false;
                }
            } 
        }

        private void btnXoaTrang_Click(object sender, EventArgs e)
        {
            MaLopXoa = txtMaLop.Text;
            Khoa();
            txtMaLop.Text = "";
            txtGVCN.Text = "";
            txtSiSo.Text = "0";
            txtKhoiLop.Text = "";
            txtLoaiKhoi.Text = "";
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            CapNhat = !CapNhat;
            string MaLopCu = MaLopXoa;
            if (txtMaLop.Text != "")
                MaLopCu = txtMaLop.Text;
            else txtMaLop.Text = MaLopCu;
            if (CapNhat == false)
            {
                dataGridViewLop.Enabled = false;
                btnThem.Enabled = false;
                btnXoa.Enabled = false;
                btnHuyCapNhat.Visible = true;
                btnCapNhat.BackColor = Color.LightGray;
                MoKhoa();
            }
            else
            {
                if (Convert.ToInt32(txtSiSo.Text) > 20)
                {
                    MessageBox.Show("Sỉ Số Vượt Quá Qui Định!");
                    CapNhat = false;
                    return;
                }

                if (txtGVCN.TextLength == 0)
                {
                    MessageBox.Show("Bạn Chưa Nhập GVCN");
                    return;
                }
                try
                {
                    
                    
                    txtLoaiKhoi.Text = txtMaLop.Text.Substring(2, 1);
                    loph.UpdateLop(MaLopCu,this.txtMaLop.Text, this.txtGVCN.Text, this.txtSiSo.Text, this.txtKhoiLop.Text, this.txtLoaiKhoi.Text);
                    MessageBox.Show("Cập Nhật Thành Công");
                    dataGridViewLop.Enabled = true;
                    Khoa();
                    btnThem.Enabled = true;
                    btnXoa.Enabled = true;
                    btnHuyCapNhat.Visible = false;
                    btnCapNhat.BackColor = Color.Transparent;
                    frmQuanLyLop_Load(null, e);
                }
                catch
                {
                    MessageBox.Show("Lớp Đã Tồn Tại Rồi!");
                }
            } 
        }
        String MaLopXoa = "";
        private void btnXoa_Click(object sender, EventArgs e)
        {

            if (txtMaLop.Text != "")
            MaLopXoa = txtMaLop.Text;
            DialogResult KQ = MessageBox.Show("Bạn Có Chắc Chắn Muốn Xóa Lớp " + MaLopXoa + "?\nNếu Xóa Lớp Tất Cả Học Sinh Thuộc Lớp "+MaLopXoa+" Sẽ Bị Xóa!", "Quyết Định!", MessageBoxButtons.YesNo);
            if (KQ == DialogResult.Yes)
            {
                DataTable DSHSXoa = new DataTable();
                //DSHSXoa = loph.DeleteHocSinhXoa(MaLopXoa);
                foreach (DataRow i in DSHSXoa.Rows)
                {
                    hs.DeleteHocSinh(i["MAHS"].ToString());
                }
                loph.DeleteLop(MaLopXoa);
                MessageBox.Show("Xóa Thành Công!");
                frmQuanLyLop_Load(null, e);
            }
        }

        private void btnHuyThemLop_Click(object sender, EventArgs e)
        {
            Khoa();
            Them = true;
            dataGridViewLop.Enabled = true;
            btnHuyThemLop.Visible = false;
            btnCapNhat.Enabled = true;
            btnXoa.Enabled = true;
            btnThem.BackColor = Color.Transparent;
            frmQuanLyLop_Load(null, e);
        }

        private void btnHuyCapNhat_Click(object sender, EventArgs e)
        {
            Khoa();
            CapNhat = true;
            dataGridViewLop.Enabled = true;
            btnThem.Enabled = true;
            btnXoa.Enabled = true;
            btnHuyCapNhat.Visible = false;
            btnCapNhat.BackColor = Color.Transparent;
            frmQuanLyLop_Load(null, e);
        }

        private void txtSiSo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

    }
}
